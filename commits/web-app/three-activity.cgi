#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson


use strict;

use GD::Simple;

print "Content-type: image/png\n";
print "Cache-Control: post-check=3600,pre-check=43200\n";
print "Cache-Control: max-age=86400\n";
print "Expires: Mon, 01 Jan 2099 05:00:00 GMT\n";
print "\n";

    # create a new image

my $ys = $ENV{QUERY_STRING};
if( -e "cache/$ys.png" ) {
    system( "cat cache/$ys.png" );
} else {
my    $img = GD::Simple->new(62,15);
my ($y1,$y2,$y3) = $ys =~ /(\d+),(\d+),(\d+)/;
    # draw a red rectangle with blue borders
    $img->bgcolor(200,200,200);
    $img->fgcolor(100,100,100);
$img->rectangle(0, (15-$y1) ,15,15);
$img->rectangle(17, (15-$y2) ,32,15);
$img->rectangle(34, (15-$y3) ,47,15);

   # convert into png data
   print $img->png;
   open CACHE, ">cache/$ys.png";
   print CACHE $img->png;
}
