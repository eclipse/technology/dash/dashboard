<?php
/* Copyright (c) 2007 IBM, made available under EPL v1.0
 * Contributors Nick Boldt
 *
 * Web app for retrieving number & size (LOC) of commits per committer/project/company.
 * For REST API (plain text) output, see ../web-api/commit-count-loc.php
 *
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 * range={1day, 1wk, 1mo, 3mo, 6mo, 9mo, 12mo, 1yr, day, month, year}
 *
 * Defaults:
 * If not specified, range will default to year.
 * If not specified, year will be set to the current year.
 *
 * If one of ?day=, ?month=, or ?year= are specified, range will be set to that value (eg., ?day=20070702 equals ?range=day&day=20070702).
 * If set range is not equal to a specified day, month, or year, range will be respected first (eg., ?range=month&year=2007 equals ?range=month (current month)).
 * If one of range={day,month,year} is set but no day, month, or year are specified, value will be current day, month or year (eg., ?range=year equals ?year=2008 while the current year is 2008).
 *
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-app/commit-count-loc.php?company=Zend%25
 * http://dash.eclipse.org/dash/commits/web-app/commit-count-loc.php?company=IBM&project=modeling%25
 *
 * TODO: verify offline/from-file version degrades properly when run on dash.eclipse.
 *
 */
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); $App = new App(); $Nav = new Nav(); $Menu = new Menu(); include($App->getProjectCommon());
ob_start();

$theme = "Phoenix";
$pageTitle = "Company / Project Commit Details";

require_once "../web-api/commit-common.inc.php";
$fields = "COMPANY, PROJECT, LOGIN";

$YMD = getYMD(); if ($debug) { print "\$YMD = "; print_r($YMD); print "<br/>\n"; }
$validRanges = getValidRanges("Ymd", $YMD);
$range = getRange(isset($_GET["range"]) ? $_GET["range"] : "year", "year", false); // defaults to year range
print $debug ? "\$range = $range<br/>\n" : ""; unset($_GET["range"]);
if (implode("", $YMD) == "")
{
	$YMD[0] = date("Y");
}
$rangeSQL = getRange($range, "YEAR = " . date("Y"), true); print $debug ? "\$rangeSQL = $rangeSQL<br/>\n" : "";
list( , , $where) = getXYWHERE(); print $debug ? "\$where = $where<br/>\n" : "";

$sortBy = null;
$show = null;
$activity_threshhold = 0;
$QS = isset($_SERVER["QUERY_STRING"]) && $_SERVER["QUERY_STRING"] ? preg_replace("#(\&|)(show|sortBy)=([^\&]+|)(\&|)#","",$_SERVER["QUERY_STRING"]) . "&" : "";

if (isset($_GET["sortBy"]))
{
	preg_match("#(activecommitters|inactivecommitters|totalcommitters|pcactive|commits|loc|alocpc)#",$_GET["sortBy"],$matches);
	if (isset($matches) && isset($matches[1]))
	{
		$sortBy=$matches[1];
	}
}
if (isset($_GET["show"]))
{
	preg_match("#(active|inactive|total|clpp)#",$_GET["show"],$matches);
	if (isset($matches) && isset($matches[1]))
	{
		$show=$matches[1];
	}
}

$query1 = "SELECT " . ($fields ? $fields . " ": "") . "FROM commits" .
	($where ? " WHERE " . $where : "") . ($fields ? " GROUP BY $fields ORDER by $fields" : ""); // this query should never have a $rangeSQL clause

$query2 = "SELECT " . ($fields ? $fields . ", " : "") . "COUNT(CHANGE_SIZE) AS NUM_COMMITS, SUM(CHANGE_SIZE) as NUM_LOC FROM commits WHERE " .
	($where ? $where . " AND " : "") . $rangeSQL . // this query should always have a $rangeSQL clause
	($fields ? " GROUP BY $fields" : "");

$api_query1 = array("http://", "dash.eclipse.org", "/dash/commits/web-api/commit-active-committers.php?${QS}queries=logins&fields=company,project,login");
#TODO api must support range too
$api_query2 = array("http://", "dash.eclipse.org", "/dash/commits/web-api/commit-count-loc.php?${QS}");

$rawdatafile = "/tmp/commit-count-loc.php.data.txt";
$subgroupfiles = array("./commit-count-loc.php.subgroups.php", "/tmp/commit-count-loc.php.subgroups.php");
foreach ($subgroupfiles as $subgroupfile)
{
	if (is_file($subgroupfile))
	{
		require_once($subgroupfile);
	}
}

$companies = array();
$commits = array();
$loc = array();
$num_committers_total = array();
$num_committers_active = array();
$num_committers_inactive = array();
$per_project = array();
$per_committer = array();
$projects = array();
$data = array();

$result1 = isset($_dbh) && $_dbh ? mysql_query($query1, $_dbh) : null;
if (!$result1 && mysql_error())
{
  	print "<p><ul><li><i>MySQL Error: " . mysql_error() . "</i></li></ul></p>\n";
}
else if (!$result1 && !mysql_error())
{
	if (is_file($rawdatafile) && !isset($_GET["refresh"]))
	{
		$result1 = loadDataFromFile($rawdatafile);
	}
	else
	{
		$result1 = loadDataFromQuery($api_query1);
		saveDataToFile($result1,$rawdatafile,"w");
	}
}

if ((is_array($result1) && sizeof($result1) > 0) || ($result1 && !mysql_error()))
{
	$cnt = 0;
	$lim = get_num_rows($result1);
	if ($debug)
	{
		print "\$query1 returned $lim rows.<br/>\n";
	}
	while($row = get_next_row($result1, $cnt))
  	{
  		$cnt++;
  		if (sizeof($row) == 3)
  		{
	  		debugRow($row, $cnt, $lim);

	  		if (!isset($data[$row[0]])) $data[$row[0]] = array();
	  		if (!isset($data[$row[0]][$row[1]])) $data[$row[0]][$row[1]] = array();
			$data[$row[0]][$row[1]][$row[2]] = array(0, 0); // all committers

			// $num_committers_total = array("ActuateCorporation" => 49, ...);
			if (!isset($num_committers_total[$row[0]])) $num_committers_total[$row[0]] = array();
			$num_committers_total[$row[0]][$row[2]] = 1; // collect everyone

			// $num_committers_active = array("ActuateCorporation" => 32, ...);
			if (!isset($num_committers_active[$row[0]])) $num_committers_active[$row[0]] = array();
			$num_committers_active[$row[0]][$row[2]] = 0; // collect everyone

			// $num_committers_inactive = array("ActuateCorporation" => 17, ...);
			if (!isset($num_committers_inactive[$row[0]])) $num_committers_inactive[$row[0]] = array();
			$num_committers_inactive[$row[0]][$row[2]] = 1; // call everyone inactive for now

			// $per_committer = array("ActuateCorporation" => array("xgu" => array(commits, LOC), ...), ...);
			if (!isset($per_committer[$row[0]])) $per_committer[$row[0]] = array();
			if (!isset($per_committer[$row[0]][$row[2]])) $per_committer[$row[0]][$row[2]] = array(0, 0);
	  	}
  	}
}

if ($debug)
{
	print "<hr noshade size=\"1\"/>\n";
}

$result2 = isset($_dbh) && $_dbh ? mysql_query($query2, $_dbh) : null;
if (!$result2 && mysql_error())
{
  	print "<p><ul><li><i>MySQL Error: " . mysql_error() . "</i></li></ul></p>\n";
}
else if (!$result2 && !mysql_error())
{
	if (is_file($rawdatafile) && !isset($_GET["refresh"]))
	{
		$result2 = $result1; // same file, already loaded
	}
	else
	{
		$result2 = loadDataFromQuery($api_query2);
		saveDataToFile($result2,$rawdatafile,"a");
	}
}

if ((is_array($result2) && sizeof($result2) > 0) || ($result2 && !mysql_error()))
{
	$cnt=0;
	$lim = get_num_rows($result2);
	if ($debug)
	{
		print "\$query2 returned $lim rows.<br/>\n";
	}
	while($row = get_next_row($result2, $cnt))
  	{
	  	$cnt++;
  		if (sizeof($row)>=5)
  		{
			debugRow($row, $cnt, $lim);

	  		/* $data[company][project][committer] = array(commits,loc) */
	  		if (!isset($data[$row[0]])) $data[$row[0]] = array();
	  		if (!isset($data[$row[0]][$row[1]])) $data[$row[0]][$row[1]] = array();
			$data[$row[0]][$row[1]][$row[2]] = array($row[3], $row[4]); // all the data

			// $commits = array("ActuateCorporation" => 36126, ...);
			if (!isset($commits[$row[0]])) $commits[$row[0]] = 0;
			$commits[$row[0]] += $row[3];

			// $loc = array("ActuateCorporation" => 1356790, ...);
			if (!isset($loc[$row[0]])) $loc[$row[0]] = 0;
			$loc[$row[0]] += $row[4];

			// $per_committer = array("ActuateCorporation" => array("xgu" => array(commits, LOC), ...), ...);
			if (!isset($per_committer[$row[0]])) $per_committer[$row[0]] = array();
			if (!isset($per_committer[$row[0]][$row[2]])) $per_committer[$row[0]][$row[2]] = array(0, 0);
			$per_committer[$row[0]][$row[2]][0] += $row[3];
			$per_committer[$row[0]][$row[2]][1] += $row[4];

			// $num_committers_total = array("ActuateCorporation" => 49, ...);
			if (!isset($num_committers_total[$row[0]])) $num_committers_total[$row[0]] = array();
			$num_committers_total[$row[0]][$row[2]] = 1; // collect everyone

			// $num_committers_active = array("ActuateCorporation" => 32, ...);
			// $num_committers_inactive = array("ActuateCorporation" => 17, ...);
			if (!isset($num_committers_active[$row[0]])) $num_committers_active[$row[0]] = array();
			if (!isset($num_committers_inactive[$row[0]])) $num_committers_inactive[$row[0]] = array();
			if ($row[3] > $activity_threshhold || $row[4] > $activity_threshhold)
			{
				$num_committers_active[$row[0]][$row[2]] =  1;
				$num_committers_inactive[$row[0]][$row[2]] = 0;
			}
			else
			{
				$num_committers_active[$row[0]][$row[2]] =  0;
				$num_committers_inactive[$row[0]][$row[2]] =  1;
			}

			// $per_project = array("ActuateCorporation" => array("birt" => array(commits, LOC), ...), ...);
			if (!isset($per_project[$row[0]])) $per_project[$row[0]] = array();
			if (!isset($per_project[$row[0]][$row[1]])) $per_project[$row[0]][$row[1]] = array(0, 0);
			$per_project[$row[0]][$row[1]][0] += $row[3];
			$per_project[$row[0]][$row[1]][1] += $row[4];
  		}
  	}
}

// $num_commits_total = 973069;
$num_commits_total = array_sum($commits);

// $num_loc_total = 23869974;
$num_loc_total = array_sum($loc);

// $num_committers_total = array("ActuateCorporation" => 49, ...);
foreach ($num_committers_total as $company => $committer_data)
{
	if (!isset($num_committers_total[$company]))
	{
		$num_committers_total[$company] = 0;
	}
	else
	{
		$num_committers_total[$company] = sizeof(array_keys($committer_data));
	}
}

// $num_committers_active = array("ActuateCorporation" => 32, ...);
foreach ($num_committers_active as $company => $committer_data)
{
	if (!isset($num_committers_active[$company]))
	{
		$num_committers_active[$company] = 0;
	}
	else
	{
		$num_committers_active[$company] = array_sum($committer_data); // use array sum instead of list of unique committers
	}
}

// $num_committers_inactive = array("ActuateCorporation" => 17, ...);
foreach ($num_committers_inactive as $company => $committer_data)
{
	if (!isset($num_committers_inactive[$company]))
	{
		$num_committers_inactive[$company] = 0;
	}
	else
	{
		$num_committers_inactive[$company] = array_sum($committer_data); // use array sum instead of list of unique committers
	}
}

// $num_committers_total_total = 855;
$num_committers_total_total = array_sum($num_committers_total);

// $num_committers_active_total = 506;
$num_committers_active_total = array_sum($num_committers_active);

// $num_committers_inactive_total = 855 - 506;
$num_committers_inactive_total = $num_committers_total_total - $num_committers_active_total;

// $num_project_committers = array("ActuateCorporation" => array("birt" => array(31, ...), ...);
$num_project_committers = array();
foreach ($data as $company => $project_data)
{
	if (!isset($num_project_committers[$company])) $num_project_committers[$company] = array();
	foreach ($project_data as $project => $committer_data)
	{
		if (!isset($num_project_committers[$company][$project])) $num_project_committers[$company][$project] = array (0, 0, 0); // active, inactive, total
		foreach ($committer_data as $pair)
		{
			if ($pair[0] > 0 || $pair[1] > 0)
			{
				$num_project_committers[$company][$project][0]++; // active
			}
			if ($pair[0] < 1 && $pair[1] < 1)
			{
				$num_project_committers[$company][$project][1]++; // inactive
			}
			$num_project_committers[$company][$project][2]++; // total
		}
	}

}

// array to use when foreaching
$array = $commits;

// calculate missing data - approx LOC per commit
$alocpc = array();
foreach ($commits as $company => $v)
{
	$alocpc[$company] = $loc[$company]/$commits[$company];
}

// calculate missing data - percent active
$percent_active = array();
foreach ($num_committers_total as $company => $v)
{
	$percent_active[$company] = $num_committers_active[$company]/$num_committers_total[$company];
}

// define sort table
if ($sortBy=="activecommitters")
{
	arsort($num_committers_active); reset($num_committers_active); $array = $num_committers_active;
}
else if ($sortBy=="inactivecommitters")
{
	arsort($num_committers_inactive); reset($num_committers_inactive); $array = $num_committers_inactive;
}
else if ($sortBy=="totalcommitters")
{
	arsort($num_committers_total); reset($num_committers_total); $array = $num_committers_total;
}
else if ($sortBy=="pcactive")
{
	arsort($percent_active); reset($percent_active); $array = $percent_active;
}
else if ($sortBy=="commits")
{
	arsort($commits); reset($commits); $array = $commits;
}
else if ($sortBy=="loc")
{
	arsort($loc); reset($loc); $array = $loc;
}
else if ($sortBy=="alocpc")
{
	arsort($alocpc); reset($alocpc); $array = $alocpc;
}

// begin rendering HTML

print "<div id=\"midcolumn\">\n";

print "<h1>$pageTitle</h1>\n";

print "<p><b><i>This automatically collected information may not represent true activity and should not be used as sole " .
	"indicator of individual or project behavior.<br/>See the <a href=\"http://wiki.eclipse.org/index.php/Commits_Explorer\">wiki page</a> for " .
	"known data anomalies. To report issues or request enhancements, see <a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=209711\">bug 209711</a>.</i></b></p>\n";

$row = 0;
// header / column sorts
print "<table><tr bgcolor=\"". bgcol($row). "\">" .
	"<th valign=\"bottom\"><a href=\"?${QS}sortBy=&amp;show=$show\">Company</a></th>" .
	"<th colspan=\"2\"><a href=\"?${QS}sortBy=activecommitters&amp;show=$show\">Active<br>Committers</a></th>" .
	"<th colspan=\"2\"><a href=\"?${QS}sortBy=inactivecommitters&amp;show=$show\">Inactive<br>Committers</a></th>" .
	"<th colspan=\"2\"><a href=\"?${QS}sortBy=totalcommitters&amp;show=$show\">Total<br>Committers</a></th>" .
	"<th colspan=\"1\"><a href=\"?${QS}sortBy=pcactive&amp;show=$show\">Percent<br>Active</a></th>" .
	"<th colspan=\"2\"><a href=\"?${QS}sortBy=commits&amp;show=$show\">Commits<br/>(" . showRange($range, $YMD) . ")</a></th>" .
	"<th colspan=\"2\"><a href=\"?${QS}sortBy=loc&amp;show=$show\">Lines of Code<br/>(" . showRange($range, $YMD) . ")</a></th>" .
	"<th colspan=\"1\"><a href=\"?${QS}sortBy=alocpc&amp;show=$show\">Approx. LOC<br/>per Commit</a></th>" .
"</tr>\n";
foreach($array as $company => $v)
{
	$row++;
	if (isset($company_subgroups) && is_array($company_subgroups) && array_key_exists($company,$company_subgroups) && isset($company_subgroups[$company]))
	{
		ksort($company_subgroups[$company]); reset($company_subgroups[$company]);

		print
			"<tr bgcolor=\"". bgcol($row). "\">" .
			"<td>$company</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_active_committers')\">" . number($num_committers_active[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_active_total ? percent($num_committers_active[$company]/$num_committers_active_total) : "0%") . ")</td>" .

			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_inactive_committers')\">" . number($num_committers_inactive[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_inactive_total ? percent(($num_committers_inactive[$company])/$num_committers_inactive_total) : "0%") . ")</td>" .

			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_total_committers')\">" . number($num_committers_total[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_total_total ? percent($num_committers_total[$company]/$num_committers_total_total) : "0%") . ")</td>" .

			"<td align=\"right\">" . percent($percent_active[$company]). "</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_cl_per_project')\">" . number($commits[$company]) . "</a></td><td align=\"right\">(" . percent($commits[$company]/$num_commits_total) . ")</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_cl_per_project')\">" . number($loc[$company]) . "</a></td><td align=\"right\">(" . percent($loc[$company]/$num_loc_total). ")</td>" .
			"<td align=\"right\">" . round($alocpc[$company]). "</td>" .
		"</tr>\n";
	}
	else
	{
		print
			"<tr bgcolor=\"". bgcol($row). "\">" .
			"<td>$company</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_active_committers')\">" . number($num_committers_active[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_active_total ? percent($num_committers_active[$company]/$num_committers_active_total) : "0%") . ")</td>" .

			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_inactive_committers')\">" . number($num_committers_inactive[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_inactive_total ? percent($num_committers_inactive[$company]/$num_committers_inactive_total) : "0%") . ")</td>" .

			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_total_committers')\">" . number($num_committers_total[$company]) . "</a></td><td align=\"right\">(" . ($num_committers_total_total ? percent($num_committers_total[$company]/$num_committers_total_total) : "0%") . ")</td>" .

			"<td align=\"right\">" . percent($percent_active[$company]). "</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_cl_per_project')\">" . (isset($commits[$company]) ? number($commits[$company]) : 0) . "</a></td><td align=\"right\">(" . (isset($commits[$company]) && $num_commits_total ? percent($commits[$company]/$num_commits_total) : "0%") . ")</td>" .
			"<td align=\"right\"><a href=\"javascript:toggle('" . $company . "_cl_per_project')\">" . (isset($loc[$company]) ? number($loc[$company]) : 0) . "</a></td><td align=\"right\">(" . (isset($loc[$company]) && $num_loc_total ? percent($loc[$company]/$num_loc_total) : "0%") . ")</td>" .
			"<td align=\"right\">" . (isset($alocpc[$company]) ? round($alocpc[$company]) : 0) . "</td>" .
		"</tr>\n";
	}
	$row++;

	ksort($per_committer[$company]); reset($per_committer[$company]);

	$arrays = array(
		"active" => 1,
		"inactive" => -1,
		"total" => 0
	);

	$row++;
	foreach ($arrays as $label => $compare)
	{
		$num_committers_wanted = "num_committers_" . $label;
		$num_committers_wanted = $$num_committers_wanted;

		$outPre = "<tr id=\"" . $company . "_" . $label . "_committers\" style=\"display:";
		$out = "\" bgcolor=\"". bgcol2($row). "\">\n";
		$out .= "<td colspan=\"13\" style=\"padding:6px\">";
		$out .= "<div style=\"float:right\"><a href=\"javascript:toggle('" . $company . "_" . $label . "_committers')\">[x]</a></div>";
		$out .= "<a href=\"javascript:toggle('" . $company . "_" . $label . "_committers')\"><b>$company: " . $num_committers_wanted[$company] . " " . ucfirst($label) . " Committers</b></a><br/>\n";
		$out .=
			"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" .
				"<tr><td valign=\"top\" style=\"padding-left:0px\">" .
					"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
		$cnt=0;
		$row2=0;
		$split_thresh = isset($num_committers_wanted[$company]) && $num_committers_wanted[$company] > 5 ? ceil($num_committers_wanted[$company]/4) : 5;
		$had_wanted_committer = false;
		foreach($per_committer[$company] as $committer_name => $committer_data)
		{
			if (compareThreshhold($committer_data, $compare))
			{
				$had_wanted_committer = true;
				$cnt++;
				if ($cnt % $split_thresh == 1)
				{
					$out .= "</table></td><td valign=\"top\" style=\"padding-left:6px\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"1\">\n";
					$out .= "<tr bgcolor=\"". bgcol($row2). "\"><th>Committer</th>" .
						($label != "inactive" ? "<th>Commmits</th><th>LOC</th>" : "") .
						"</tr>\n";
					$row2++;
				}
				$out .=
					"<tr bgcolor=\"". bgcol($row2). "\"> <td valign=\"top\" align=\"left\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
					"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?project=x&type=y&range=" . $range . "&login=" . $committer_name . "\">" . $committer_name . "</a>" .
					($label != "inactive" ?
						"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\">" . number($committer_data[0]) .
						"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\">" . number($committer_data[1])
					: "") .
					"</td> </tr>\n";
				$row2++;
			}
		}
		if (!$had_wanted_committer)
		{
			$out .= "<i>No " . ($compare ? $label : "") . " committers.</i>";
		}
		$out .= "</table></td></tr></table>";
		$out .= "</td></tr>\n";
		print $outPre . ($show == $label && $had_wanted_committer ? "" : "none") . $out; // hide rows if no data (eg., inactive committers)
		$row++;
	}

	// commits & loc by project
	if (isset($per_project[$company]))
	{
		print "<tr id=\"" . $company . "_cl_per_project\" style=\"display:" . ($show == "clpp" ? "" : "none") . "\" bgcolor=\"". bgcol2($row). "\">\n";
		print "<td colspan=\"13\" style=\"padding:6px\">";
		print "<div style=\"float:right\"><a href=\"javascript:toggle('" . $company . "_cl_per_project')\">[x]</a></div>";
		print "<a href=\"javascript:toggle('" . $company . "_cl_per_project')\"><b>$company: LOC &amp; Committer Count for " . (isset($per_project[$company]) ? sizeof($per_project[$company]) : 0) . " Projects</b></a><br/>\n";
		print
			"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" .
				"<tr><td valign=\"top\" style=\"padding-left:0px\">" .
					"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
		$cnt=0;
		$row2=0;
		$split_thresh = sizeof($per_project[$company]) > 5 ? ceil(sizeof($per_project[$company])/2) : 5;
		foreach($per_project[$company] as $project_name => $per_project_pair)
		{
			$cnt++;
			if ($cnt % $split_thresh == 1)
			{
				print "</table></td><td valign=\"top\" style=\"padding-left:6px\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"1\">\n";
				print "<tr bgcolor=\"". bgcol($row2). "\"><th style=\"padding-bottom:4px\">Project</th>
					<th colspan=\"3\" style=\"padding-bottom:4px\"><acronym title=\"Active, Inactive, Total Committers\">Committers</acronym></th>
					<th style=\"padding-bottom:4px\">Commits</th>
					<th style=\"padding-bottom:4px\"><acronym title=\"Lines of Code\">LOC</acronym></th></tr>\n";
				$row2++;
			}
			print
				"<tr bgcolor=\"". bgcol($row2). "\"> <td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
					"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?company=x&login=y&project=" . $project_name . "&range=" . $range . "\">" . $project_name . "</a>" .
				"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
					"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?top=x&company=" . $company . "&login=y&project=" . $project_name . "&range=" . $range . "\">" .
						number($num_project_committers[$company][$project_name][0]) . "</a>" .
				"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\">" .
						number($num_project_committers[$company][$project_name][1]) .
				"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
					"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?top=x&company=" . $company . "&login=y&project=" . $project_name . "\">" .
						number($num_project_committers[$company][$project_name][2]) . "</a>" .
				"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
					"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?company=x&month=y&project=" . $project_name . "&range=" . $range . "\">" . number($per_project_pair[0]) . "</a>" .
				"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\"><a href=\"" .
					"http://dash.eclipse.org/dash/commits/web-api/commit-count-loc.php?project=" . $project_name . "&range=" . $range . "\">" .  number($per_project_pair[1]) . "</a>" .
				"</td> </tr>\n";
			$row2++;
		}
		if (!$cnt)
		{
			print "<tr><td colspan=\"4\"><i>No committers, commits, or LOC.</i></td></tr>\n";
		}
		print "</table></td></tr></table>";
		print "</td></tr>\n";
	}
	$row++;

	if (isset($company_subgroups) && is_array($company_subgroups) && array_key_exists($company,$company_subgroups) && isset($company_subgroups[$company]))
	{
		$num_subgroup_committers_active = array();
		$num_subgroup_committers_inactive = array();
		$num_subgroup_committers_total = array();
		$subgroup_commits = array();
		$subgroup_loc = array();
		$subgroup_alocpc = array();

		foreach($company_subgroups[$company] as $subgroup_name => $subgroup_list)
		{
			$subgroup_label = $company . "_" . preg_replace("#[,&\(\) ]+#", "_",$subgroup_name);
			$num_subgroup_committers_total[$company] = 0;
			$num_subgroup_committers_active[$company] = 0;
			$num_subgroup_committers_inactive[$company] = 0;

			$subgroup_commits[$company] = 0;
			$subgroup_loc[$company] = 0;
			$num_subgroup_commits_total = 0;
			$num_subgroup_loc_total = 0;
			$num_subgroup_commits_total = $commits[$company];
			$num_subgroup_loc_total = $loc[$company];
			$subgroup_alocpc[$company] = 0;

			$per_subgroup_project = array();
			$per_subgroup_project[$company] = array();

			$num_subgroup_project_committers = array();
			if (!isset($num_subgroup_project_committers[$company])) $num_subgroup_project_committers[$company] = array();

			foreach ($subgroup_list as $n)
			{
				$num_subgroup_committers_total[$company] 	+= isset($per_committer[$company][$n]) ? 1 : 0;
				$num_subgroup_committers_active[$company]   += isset($per_committer[$company][$n]) && compareThreshhold($per_committer[$company][$n], 1) ? 1 : 0;
				$num_subgroup_committers_inactive[$company] += isset($per_committer[$company][$n]) && compareThreshhold($per_committer[$company][$n], -1) ? 1 : 0;
				$subgroup_commits[$company] += isset($per_committer[$company][$n]) ? $per_committer[$company][$n][0] : 0;
				$subgroup_loc[$company] += isset($per_committer[$company][$n]) ? $per_committer[$company][$n][1] : 0;
				foreach ($per_project[$company] as $project_name => $project_data)
				{
					if (!isset($per_subgroup_project[$company][$project_name])) $per_subgroup_project[$company][$project_name] = array(0, 0);
					$per_subgroup_project[$company][$project_name][0] += isset($data[$company][$project_name][$n]) ? $data[$company][$project_name][$n][0] : 0;
					$per_subgroup_project[$company][$project_name][1] += isset($data[$company][$project_name][$n]) ? $data[$company][$project_name][$n][1] : 0;
					if (!isset($num_subgroup_project_committers[$company][$project_name])) $num_subgroup_project_committers[$company][$project_name] = array(0, 0, 0);
					$num_subgroup_project_committers[$company][$project_name][0] += isset($data[$company][$project_name][$n]) && ($data[$company][$project_name][$n][0] > 0 || $data[$company][$project_name][$n][1] > 0) ? 1 : 0; // active
					$num_subgroup_project_committers[$company][$project_name][1] += isset($data[$company][$project_name][$n]) && $data[$company][$project_name][$n][0] < 1 && $data[$company][$project_name][$n][1] < 1   ? 1 : 0; // inactive
					$num_subgroup_project_committers[$company][$project_name][2] += isset($data[$company][$project_name][$n]) ? 1 : 0; // total
				}
			}

			// suppress empty rows
			if (isset($num_subgroup_committers_total[$company]) && $num_subgroup_committers_total[$company] > 0)
			{
				$subgroup_alocpc[$company] = $subgroup_commits[$company] > 0 ? $subgroup_loc[$company]/$subgroup_commits[$company] : 0;
				print
					"<tr bgcolor=\"". bgcol3($row). "\">" .
					"<td><i>&#160;&#160;+ " . $subgroup_name . "</i></td>" .
					"<td align=\"right\"><i><a href=\"javascript:toggle('" . $subgroup_label . "_active_committers')\">" . number($num_subgroup_committers_active[$company]) . "</a></i></td><td align=\"right\"><i>(" . ($num_committers_active[$company] ? percent($num_subgroup_committers_active[$company]/$num_committers_active[$company]) : "0%") . ")</i></td>" .

					"<td align=\"right\"><i><a href=\"javascript:toggle('" . $subgroup_label . "_inactive_committers')\">" . number($num_subgroup_committers_inactive[$company]) . "</a></i></td><td align=\"right\"><i>(" . ($num_committers_inactive[$company] ? percent($num_subgroup_committers_inactive[$company]/$num_committers_inactive[$company]) : "0%") . ")</i></td>" .

					"<td align=\"right\"><i><a href=\"javascript:toggle('" . $subgroup_label . "_total_committers')\">" . number($num_subgroup_committers_total[$company]) . "</a></i></td><td align=\"right\"><i>(" . ($num_committers_total[$company] ? percent($num_subgroup_committers_total[$company]/$num_committers_total[$company]) : "0%") . ")</i></td>" .

					"<td align=\"right\"><i>" . ($num_subgroup_committers_total[$company] ? percent($num_subgroup_committers_active[$company]/$num_subgroup_committers_total[$company]) : "0%") . "</td>" .
					"<td align=\"right\"><a href=\"javascript:toggle('" . $subgroup_label . "_cl_per_project')\">" . number($subgroup_commits[$company]) . "</a></td><td align=\"right\">(" . ($num_subgroup_commits_total ? percent($subgroup_commits[$company]/$num_subgroup_commits_total) : "0%") . ")</td>" .
					"<td align=\"right\"><a href=\"javascript:toggle('" . $subgroup_label . "_cl_per_project')\">" . number($subgroup_loc[$company]) . "</a></td><td align=\"right\">(" . ($num_subgroup_loc_total ? percent($subgroup_loc[$company]/$num_subgroup_loc_total) : "0%") . ")</td>" .
					"<td align=\"right\">" . round($subgroup_alocpc[$company]). "</td>" .
							"</tr>\n";

				ksort($per_committer[$company]); reset($per_committer[$company]);

				$arrays = array(
					"active" => 1,
					"inactive" => -1,
					"total" => 0
				);

				foreach ($arrays as $label => $compare)
				{
					$num_committers_wanted = "num_subgroup_committers_" . $label;
					$num_committers_wanted = $$num_committers_wanted;

					$outPre = "<tr id=\"" . $subgroup_label . "_" . $label . "_committers\" style=\"display:";
					$out = "\" bgcolor=\"". bgcol2($row). "\">\n";
					$out .= "<td colspan=\"13\" style=\"padding:6px\">";
					$out .= "<div style=\"float:right\"><a href=\"javascript:toggle('" . $subgroup_label . "_" . $label . "_committers')\">[x]</a></div>";
					$out .= "<a href=\"javascript:toggle('" . $subgroup_label . "_" . $label . "_committers')\"><b>$company: " . $num_committers_wanted[$company] . " " . ucfirst($label) . " Committers</b></a><br/>\n";
					$out .=
						"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" .
							"<tr><td valign=\"top\" style=\"padding-left:0px\">" .
								"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
					$cnt=0;
					$row2=0;

					$split_thresh = isset($num_committers_wanted[$company]) && $num_committers_wanted[$company] > 5 ? ceil($num_committers_wanted[$company]/4) : 5;
					$had_wanted_committer = false;

					foreach($per_committer[$company] as $committer_name => $committer_data)
					{
						if (in_array($committer_name, $subgroup_list) && compareThreshhold($committer_data, $compare))
						{
							$had_wanted_committer = true;
							$cnt++;
							if ($cnt % $split_thresh == 1)
							{
								$out .= "</table></td><td valign=\"top\" style=\"padding-left:6px\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"1\">\n";
								$out .= "<tr bgcolor=\"". bgcol($row2). "\"><th>Committer</th>" .
									($label != "inactive" ? "<th>Commmits</th><th>LOC</th>" : "") .
									"</tr>\n";
								$row2++;
							}
							$out .=
								"<tr bgcolor=\"". bgcol($row2). "\"> <td valign=\"top\" align=\"left\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
								"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?project=x&type=y&range=" . $range . "&login=" . $committer_name . "\">" . $committer_name . "</a>" .
								($label != "inactive" ?
									"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\">" . number($committer_data[0]) .
									"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\">" . number($committer_data[1])
								: "") .
								"</td> </tr>\n";
							$row2++;
						}
					}
					if (!$had_wanted_committer)
					{
						$out .= "<i>No " . ($compare ? $label : "") . " committers.</i>";
					}
					$out .= "</table></td></tr></table>";
					$out .= "</td></tr>\n";
					print $outPre . ($show == $label && $had_wanted_committer ? "" : "none") . $out; // hide rows if no data (eg., inactive committers)
					$row++;
				}

				// commits & loc by project
				if (isset($per_subgroup_project[$company]))
				{
					$outPre = "<tr id=\"" . $subgroup_label . "_cl_per_project\" style=\"display:" . ($show == "clpp" ? "" : "none") . "\" bgcolor=\"". bgcol2($row). "\">\n";
					$outPre .= "<td colspan=\"13\" style=\"padding:6px\">";
					$outPre .= "<div style=\"float:right\"><a href=\"javascript:toggle('" . $subgroup_label . "_cl_per_project')\">[x]</a></div>";
					$outPre .= "<a href=\"javascript:toggle('" . $subgroup_label . "_cl_per_project')\"><b>" . str_replace("amp;", "& ", str_replace("_", " ", $subgroup_label)) . ": LOC &amp; Committer Count for ";
					$out = " Projects</b></a><br/>\n";
					$out .=
						"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">" .
							"<tr><td valign=\"top\" style=\"padding-left:0px\">" .
								"<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\n";
					$cnt=0;
					$row2=0;

					foreach($per_subgroup_project[$company] as $project_name => $per_project_pair)
					{
						if (is_array($per_project_pair) && sizeof($per_project_pair) == 2 && ($num_subgroup_project_committers[$company][$project_name][2] > 0 || $per_project_pair[0] > 0 || $per_project_pair[1] > 0))
						{
							$cnt++;
						}
					}
					$split_thresh = $cnt > 5 ? ceil($cnt/2) : 5;

					$cnt=0;
					foreach($per_subgroup_project[$company] as $project_name => $per_project_pair)
					{
						if (is_array($per_project_pair) && sizeof($per_project_pair) == 2 && ($num_subgroup_project_committers[$company][$project_name][2] > 0 || $per_project_pair[0] > 0 || $per_project_pair[1] > 0))
						{
							$cnt++;
							if ($cnt % $split_thresh == 1)
							{
								$out .= "</table></td><td valign=\"top\" style=\"padding-left:6px\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"1\">\n";
								$out .= "<tr bgcolor=\"". bgcol($row2). "\"><th style=\"padding-bottom:4px\">Project</th>
									<th colspan=\"3\" style=\"padding-bottom:4px\"><acronym title=\"Active, Inactive, Total Committers\">Committers</acronym></th>
									<th style=\"padding-bottom:4px\">Commits</th>
									<th style=\"padding-bottom:4px\"><acronym title=\"Lines of Code\">LOC</acronym></th></tr>\n";
								$row2++;
							}
							$out .=
								"<tr bgcolor=\"". bgcol($row2). "\"> <td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
									"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?company=x&login=y&project=" . $project_name . "&range=" . $range . "\">" . $project_name . "</a>" .
								"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
									"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?top=x&company=" . $company . "&login=y&project=" . $project_name . "&range=" . $range . "\">" .
										number($num_subgroup_project_committers[$company][$project_name][0]) . "</a>" .
								"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\">" .
										number($num_subgroup_project_committers[$company][$project_name][1]) .
								"</td><td valign=\"top\" align=\"center\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
									"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?top=x&company=" . $company . "&login=y&project=" . $project_name . "\">" .
										number($num_subgroup_project_committers[$company][$project_name][2]) . "</a>" .
								"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\"><a target=\"summary\" href=\"" .
									"http://dash.eclipse.org/dash/commits/web-app/summary.cgi?company=x&month=y&project=" . $project_name . "&range=" . $range . "\">" . number($per_project_pair[0]) . "</a>" .
								"</td><td valign=\"top\" align=\"right\" style=\"padding-left:6px\"><a href=\"" .
									"http://dash.eclipse.org/dash/commits/web-api/commit-count-loc.php?project=" . $project_name . "&range=" . $range . "\">" .  number($per_project_pair[1]) . "</a>" .
								"</td> </tr>\n";
							$row2++;
						}
					}
					if (!$cnt)
					{
						$out.= "<tr><td colspan=\"4\"><i>No committers, commits, or LOC.</i></td></tr>\n";
					}
					$out .= "</table></td></tr></table>";
					$out .= "</td></tr>\n";
					print $outPre . $cnt . $out;
				}
				$row++;
			} // end if total committers > 0 (suppress empty rows)
		}
	}

}

// footer / totals
$row++;
print "<tr bgcolor=\"". bgcol($row). "\">" .
		"<th>Total</th>" .
		"<th colspan=\"1\" align=\"right\">".number($num_committers_active_total)."</th><th colspan=\"1\"></th>" .
		"<th colspan=\"1\" align=\"right\">".number($num_committers_inactive_total)."</th><th colspan=\"1\"></th>" .
		"<th colspan=\"1\" align=\"right\">".number($num_committers_total_total)."</th><th colspan=\"1\"></th>" .
		"<th colspan=\"1\"></th>" .
		"<th colspan=\"1\" align=\"right\">".number($num_commits_total)."</th><th colspan=\"1\"></th>" .
		"<th colspan=\"1\" align=\"right\">".number($num_loc_total)."</th><th colspan=\"1\"></th>" .
		"<th colspan=\"1\"></th>" .
	"</tr>\n";
print "</table>\n";

print "<p><i><a href=\"" . implode($api_query1) . "\">#</a> $query1<br/>\n";
print "<a href=\"" . implode($api_query2) . "\">#</a> $query2</i></p>\n";

print "<p>Notes: <ul><li>An inactive committer is defined as someone with zero commits and zero LOC.</li></ul></p>\n";

print "<p>&#160;</p>\n";

print "</div>\n"; // midcolumn

print "<div id=\"rightcolumn\">\n";

print "<div class=\"sideitem\">\n";
print "<h6>About</h6>\n";
print "<p>Updated:<br/>" . date("Y-m-d H:i T", (!isset($_GET["refresh"]) && is_file($rawdatafile) ? filemtime($rawdatafile) : strtotime("now"))) .
	(is_file($rawdatafile) ? " :: <a href=\"?${QS}sortBy=$sortBy&amp;show=$show&amp;refresh\">Refresh</a>" : "") . "</p>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>Sort By</h6>\n";
print "<ul>\n";
print "<li><a " . ($sortBy == "" ? "name" : "href") . "=\"?${QS}sortBy=&amp;show=$show\">Company Name</a></li>\n";
print "<li><a " . ($sortBy == "activecommitters" ? "name" : "href") . "=\"?${QS}sortBy=activecommitters&amp;show=$show\">Active Committers</a></li>\n";
print "<li><a " . ($sortBy == "inactivecommitters" ? "name" : "href") . "=\"?${QS}sortBy=inactivecommitters&amp;show=$show\">Inactive Committers</a></li>\n";
print "<li><a " . ($sortBy == "totalcommitters" ? "name" : "href") . "=\"?${QS}sortBy=totalcommitters&amp;show=$show\">Total Committers</a></li>\n";
print "<li><a " . ($sortBy == "active" ? "name" : "href") . "=\"?${QS}sortBy=pcactive&amp;show=$show\">Percent Active</a></li>\n";
print "<li><a " . ($sortBy == "commits" ? "name" : "href") . "=\"?${QS}sortBy=commits&amp;show=$show\">Commits</a></li>\n";
print "<li><a " . ($sortBy == "loc" ? "name" : "href") . "=\"?${QS}sortBy=loc&amp;show=$show\">Lines of Code</a></li>\n";
print "<li><a " . ($sortBy == "alocpc" ? "name" : "href") . "=\"?${QS}sortBy=alocpc&amp;show=$show\">Approx. LOC per Commit</a></li>\n";
print "</ul>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>Show</h6>\n";
print "<ul>\n";
print "<li><a " . ($show == "active" ? "name" : "href") . "=\"?${QS}sortBy=$sortBy&amp;show=active\">Active Committers</a></li>\n";
print "<li><a " . ($show == "inactive" ? "name" : "href") . "=\"?${QS}sortBy=$sortBy&amp;show=inactive\">Inactive Committers</a></li>\n";
print "<li><a " . ($show == "total" ? "name" : "href") . "=\"?${QS}sortBy=$sortBy&amp;show=total\">Total Committers</a></li>\n";
print "<li><a " . ($show == "clpp" ? "name" : "href") . "=\"?${QS}sortBy=$sortBy&amp;show=clpp\">Per-Project Commits &amp; LOC</a></li>\n";
print "<li><a " . ($show == "" ? "name" : "href") . "=\"?${QS}sortBy=$sortBy&amp;show=\">No Details</a></li>\n";
print "</ul>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>Filter</h6>\n";
print "<p>You can filter what data is being queried by use of the following querystring fields:</p>\n";
print "<ul>\n";
print "<li><a href=\"?${QS}top=modeling\"><b>top</b>=<i>topProjectName</i></a></li>\n";
print "<li><a href=\"?${QS}project=modeling%\"><b>project</b>=<i>projectName</i></a></li>\n";
print "<li><a href=\"?${QS}year=2007\"><b>year</b>=<i>yyyy</i></a></li>\n";
print "<li><a href=\"?${QS}month=200801\"><b>month</b>=<i>yyyymm</i></a></li>\n";
print "<li><a href=\"?${QS}day=20080107\"><b>day</b>=<i>yyyymmdd</i></a></li>\n";
print "<li><a href=\"?${QS}login=nickb\"><b>login</b>=<i>loginName</i></a></li>\n";
print "<li><b>file</b>=<i>fileName</i></a></li>\n";
print "<li><b>type</b>=<i>fileType</i></a></li>\n";
print "<li><a href=\"?${QS}company=IBM\"><b>company</b>=<i>companyName</i></a></li>\n";
print "<li><a href=\"?${QS}range=12mo\"><b>range</b>=<i>{1day, 1wk, 1mo,<br/>3mo, 6mo, 9mo, 12mo, 1yr,<br/>day, month, year}</i></a></li>\n";

print "</ul>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>Data</h6>\n";
print "<p><ul>\n";
print "<li><a href=\"../web-api/commit-count-loc.php?${QS}\">Live data</a> via <a href=\"http://dev.eclipse.org/viewcvs/index.cgi/org.eclipse.dash/dashboard/commits/web-api/commit-count-loc.php?root=Technology_Project&view=markup\">REST API</a></li>\n";
if (is_file($rawdatafile))
{
	print "<li><a href=\"commit-count-loc.php.data.txt\">Cached Data</a></li>\n";

}
print "</ul></p>\n";
print "</div>\n";

print "</div>\n"; // rightcolumn

print "</div>\n";

$html = ob_get_contents();
ob_end_clean();

$pageKeywords = "";
$pageAuthor = "Nick Boldt";

$App->AddExtraHtmlHeader('<script src="includes/toggle.js" type="text/javascript"></script>' . "\n"); //ie doesn't understand self closing script tags, and won't even try to render the page if you use one
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

function number($num)
{
	return number_format($num);
}

function percent($num)
{
	$val = (round($num*10000)/100);
	return $val . "%";
}

function bgcol($row)
{
	return $row % 2 == 0 ? "#EEEEEE" : "#FFFFFF";
}

function bgcol2($row)
{
	return $row % 2 == 0 ? "#EEEEEE" : "#DDDDDD";
}
function bgcol3($row)
{
	return $row % 2 == 0 ? "#EEEEFF" : "#DDDDFF";
}

function compareThreshhold($committer_data, $compare)
{
	global $activity_threshhold;
	if ($compare === 0) return true; // total
	if ($compare > 0 && ($committer_data[0] > $activity_threshhold || $committer_data[1] > $activity_threshhold)) return true; // active
	if ($compare < 0 && $committer_data[0] <= $activity_threshhold && $committer_data[1] <= $activity_threshhold) return true; // inactive
	return false;
}

function debugRow(&$row, &$cnt, &$lim, $debug_lim = 1)
{
	global $debug;
	if ($debug > $debug_lim)
	{
		print "<tt>[" . str_pad($cnt, 3, "0", STR_PAD_LEFT) . "/" . str_pad($lim, 3, "0", STR_PAD_LEFT) . "] ";
		for ($i=0; $i<sizeof($row); $i++)
		{
			print (isset($row[$i]) ? $row[$i] . "\t" : "");
		}
		print "</tt><br/>\n";
	}
}

function get_num_rows($result)
{
	return is_array($result) ? sizeof($result) : mysql_num_rows($result);
}

function get_next_row($result, $pointer)
{
	return is_array($result) ? (isset($result[$pointer]) ? $result[$pointer] : null) : mysql_fetch_row($result);
}

function loadDataFromFile($file)
{
	global $debug;
	if ($debug > 0)	print "Loading data from " . $file . "...<br/>\n";
	$rawdata = file($file);
	return parseDataToArray($rawdata);
}

function loadDataFromQuery($query)
{
	global $debug, $rawdatafile;
	if ($debug > 0)	print "Loading data from " . implode($query) . "...<br/>\n";
	$rawdata = http_file($query[1], $query[2]);
	return parseDataToArray($rawdata);
}

function saveDataToFile($array, $file, $mode)
{
	$fp = fopen($file, $mode);
	fputs($fp, "# saved " . date("c") . "\n\n");
	foreach ($array as $row)
	{
		for ($i=0; $i<sizeof($row); $i++)
		{
			fputs($fp, isset($row[$i]) ? $row[$i] . "\t" : "");
		}
		fputs($fp, "\n");
	}
	fclose($fp);
}

function parseDataToArray($array)
{
	global $debug;
	$return = array();
	$loading = false;
	foreach($array as $k => $line)
	{
		if (preg_match("/^#/",$line)) // omit comments
		{
			$loading = true;
			if ($debug > 2) print "<pre>$line</pre>\n";
		}
		else if ($loading && trim($line))
		{
			$return[] = explode("\t",trim($line));
		}
	}
	if ($debug > 0)	print "Loaded " . sizeof($return) . " lines of data.<br/>\n";
	return $return;
}

function showRange($range, $YMD)
{
	if ($range == "year")
	{
		return $YMD[0];
	}
	else if ($range == "month")
	{
		return $YMD[1];
	}
	else if ($range == "day")
	{
		return $YMD[2];
	}
	return $range;
} ?>