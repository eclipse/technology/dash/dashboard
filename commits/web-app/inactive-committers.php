<html><head>
<META NAME="ROBOTS" CONTENT="NOINDEX,NOFOLLOW">
</head><body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr style="background-image: url(header_bg.gif);">
<td>
<a href="http://www.eclipse.org/"><img src="header_logo.gif" width="163" height="68" border="0" alt="Eclipse Logo" class="logo" /></a>
</td>
<td align="right" style="color: white; font-family: verdana,arial,helvetica; font-size: 1.25em; font-style: italic;"><b>Eclipse dashboard&nbsp;</b> </td>
</tr>
</table>

<?php
$project2person2status = array();
/*
 * Get all the committers for all the projects
 * Default state = inactive
 */
$data = file( 'http://www.eclipse.org/projects/web-api/commit-projects-committers.php' );
foreach( $data as $line ) {
	$x = explode( "\t", $line );
	$projectid = trim($x[0]);
	$personid = trim($x[1]);
	$relation = trim($x[2]);
	if( $relation == 'CM' )
		$project2person2status[ $projectid ][ $personid ] = 'inactive';
}
/*
 * Get the commits information for each committer to determine active/participating status
 */
$data = file("http://dash.eclipse.org/dash/commits/web-api/commits-index.php");
$counts = array();
foreach( $data as $line ) {
	$words = split( "\t", $line );
	if( substr($words[0],0,1) == '#' ) {
		continue;
	}
	$personid = trim($words[0]);
	$projectid = trim($words[3]);
	if( isset($project2person2status[ $projectid ][ $personid ]) ) {
		if( $words[2] == 3 && $words[1] > 0 ) {
			$project2person2status[ $projectid ][ $personid ] = 'active';
		} else if( $words[2] == 9 && $words[1] > 0 ) {
			$project2person2status[ $projectid ][ $personid ] = 'participating';
		}
	}
}
/*
 * Get all the set-active ids and mark them as 'active' (obviously)
 */
$data = file( 'http://www.eclipse.org/projects/web-api/commit-set-actives.php' );
foreach( $data as $line ) {
	$x = explode( "\t", $line );
	$personid = trim($x[0]);
	$projectid = trim($x[1]);
	$project2person2status[ $projectid ][ $personid ] = 'active';
}
/*
 * Compute the percentages by project
 */
$records = array();
foreach( array_keys( $project2person2status ) as $projectid ) {
	$cnt = array();
	foreach( $project2person2status[$projectid] as $personid => $status ) {
		$cnt[$status] += 1;
	}
	$total = $cnt['active'] + $cnt['participating'] + $cnt['inactive'];
	$rec = array(
		'inactivecnt' => $cnt['inactive'],
		'active' => $cnt['active'] / $total,
		'participating' => $cnt['participating'] / $total,
		'inactive' => $cnt['inactive'] / $total,
		'projectid' => $projectid );
	$records[] = $rec;
}
/*
 * Sort the records
 */
function cmprecbypercent($a, $b)
{
    if ($a === $b) {
        return 0;
    }
    if( $a['inactive'] > $b['inactive'] ) return 1;
    if( $a['inactive'] < $b['inactive'] ) return -1;
    if( $a['participating'] > $b['participating'] ) return 1;
    if( $a['participating'] < $b['participating'] ) return -1;
    if( $a['active'] > $b['active'] ) return 1;
    if( $a['active'] < $b['active'] ) return -1;
    if( $a['projectid'] > $b['projectid'] ) return 1;
    if( $a['projectid'] < $b['projectid'] ) return -1;
    return 0;
}
function cmprecbycnt($a, $b)
{
    if ($a === $b) {
        return 0;
    }
    if( $a['inactivecnt'] > $b['inactivecnt'] ) return 1;
    if( $a['inactivecnt'] < $b['inactivecnt'] ) return -1;
    if( $a['inactive'] > $b['inactive'] ) return 1;
    if( $a['inactive'] < $b['inactive'] ) return -1;
    if( $a['participating'] > $b['participating'] ) return 1;
    if( $a['participating'] < $b['participating'] ) return -1;
    if( $a['active'] > $b['active'] ) return 1;
    if( $a['active'] < $b['active'] ) return -1;
    if( $a['projectid'] > $b['projectid'] ) return 1;
    if( $a['projectid'] < $b['projectid'] ) return -1;
    return 0;
}
if( isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'cnt' ) {
	usort( $records, 'cmprecbycnt' );
} else {
	usort( $records, 'cmprecbypercent' );
}
/*
 * Display the results
 */
?>
<h2>Percentage of Active Committers</h2>
<p>
Sort by
<?php
if( isset($_REQUEST['sort']) && $_REQUEST['sort'] == 'cnt' ) {
	?><a href="?sort=percent">percent</a> <a href="?sort=cnt"><font color='gray'>cnt</font></a>
	<?php
} else {
	?><a href="?sort=percent"><font color='gray'>percent</font></a> <a href="?sort=cnt">cnt</a>
	<?php
}
?>
</p>
<table>
<?php
foreach( $records as $rec ) {
?><tr><td><a href="http://www.eclipse.org/projects/project_summary.php?projectid=<?= $rec['projectid'] ?>"><?= $rec['projectid'] ?></a></td>
<td style="background-color: #AFA; width: 500px; margin-bottom: 2px">
<?php if( $rec['inactive'] > 0 ) { ?>
<div style="float: left; background-color: #F66; width: <?= floor($rec['inactive'] * 500) ?>px; text-align: right; font-weight: bold"><?= floor($rec['inactive'] * 100) ?>%&nbsp;</div>
<?php } if( $rec['participating'] > 0 ) { ?>
<div style="float: left; background-color: #AAF; width: <?= floor($rec['participating'] * 500) ?>px; text-align: right; font-weight: bold"><?= floor($rec['participating'] * 100) ?>%&nbsp;</div>
<?php } ?>
</td>
</tr>
<?php
}
?>
</table>
<table width=500><tr><td>
<font size=-1 color=gray>
This automatically collected information may not represent
true activity and should not be used as sole indicator of
individual or project behavior.
See the <a href="http://wiki.eclipse.org/index.php/Commits_Explorer">wiki page</a> about known data anamolies.
</font>
</table>

