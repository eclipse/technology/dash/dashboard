<?php
/*
 * This is a sample file. 
 * 
 * Rename this file to commit-count-loc.subgroups.php and place it in this directory
 * or in /tmp so that the script commit-count-loc.php can find it
 * 
 * This file, once renamed, will allow you to provide subgroup data about your company, 
 * in order to see how the parts of your company contribute to Eclipse. 
 * 
 * Mapping is done by comitterid (the LOGIN field); assign your staff`s ids to the 
 * appropriate groups. $committer_emails is not required, but may be helpful if 
 * you have a database mapping your staff`s email addresses to their groups, rather
 * than their Eclipse committerids to your groups. 
 * 
 * With this information, you can effectively say:
 * 
 * Eclipse :: committerid :: email address :: company subgroup
 * 
 */
$committer_emails = array(
	"MyCo" => array(
		"committerid1" => "committer.id1@myco.com",
		"committerid2" => "committer.id2@myco.com",
		"defunctid1" => null,
	),
);

$company_subgroups = array(
	"MyCo" => array(
		"Sub Group One" => array(
			"committerid1",
		),
		"Sub Group Two" => array(
			"committerid2",
		),
	),
);
?>