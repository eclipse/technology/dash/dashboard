#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#

use strict;

use GD::Simple;

print "Content-type: image/png\n";
print "Cache-Control: post-check=3600,pre-check=43200\n";
print "Cache-Control: max-age=86400\n";
print "Expires: Mon, 01 Jan 2099 05:00:00 GMT\n";
print "\n";

    # create a new image

my $_ys = $ENV{QUERY_STRING};
$_ys =~ /(\d+(,\d+)*)/;
my $ys = $1;
my @ys = split( /,/, $ys );
my $width = 8 * scalar( @ys );
my    $img = GD::Simple->new($width,20);
    $img->transparent();
    $img->clear();
    $img->fgcolor(100,100,100);
    $img->bgcolor(200,200,200);

my $x = 0;
foreach my $y ( @ys ) {
  $img->rectangle( $x, 20-$y, $x+6, 20);
  $x += 8;
}

   # convert into png data
   print $img->png;

