#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#

use strict;

use GD::Simple;

print "Content-type: image/png\n";
print "Cache-Control: post-check=3600,pre-check=43200\n";
print "Cache-Control: max-age=86400\n";
print "Expires: Mon, 01 Jan 2099 05:00:00 GMT\n";
print "\n";

    # create a new image

my $_ys = $ENV{QUERY_STRING};
$_ys =~ /(\d+(,\d+)*);(\d+(,\d+)*)/;
my $ys = $1;
my @ys = split( /,/, $ys );
my $zs = $3;
my @colors = split( /,/, $zs );

my    $img = GD::Simple->new(50,50);
    $img->transparent();
    $img->clear();
    $img->fgcolor(100,100,100);
    $img->bgcolor(200,200,200);

$img->moveTo(25,25);
#$img->ellipse(48,48);
my $degrees = 0;
foreach my $y ( @ys ) {
	my $r = shift @colors;
	my $g = shift @colors;
	my $b = shift @colors;
    	$img->bgcolor($r,$g,$b);
	my $end = $y * 3.6 + $degrees;	
	if ($degrees != $end) {
		$img->arc(48,48,$degrees,$end);
	}
	$degrees = $end;
}
#my $x = 0;
#foreach my $y ( @ys ) {
#  $img->rectangle( $x, 20-$y, $x+6, 20);
#  $x += 8;
#}

   # convert into png data
   print $img->png;

