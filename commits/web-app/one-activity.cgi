#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#

use strict;

use GD::Simple;

print "Content-type: image/png\n";
print "Cache-Control: post-check=3600,pre-check=43200\n";
print "Cache-Control: max-age=86400\n";
print "Expires: Mon, 01 Jan 2099 05:00:00 GMT\n";
print "\n";

    # create a new image

my $ys = $ENV{QUERY_STRING};
my    $img = GD::Simple->new(17,15);
my ( $y1 ) = $ys =~ /(\d+)/;
    $img->bgcolor(200,200,200);
    $img->fgcolor(100,100,100);
$img->rectangle(0, (15-$y1) ,15,15);

   # convert into png data
   print $img->png;
open CACHE, ">images/15bar${y1}.png";
print CACHE $img->png;

