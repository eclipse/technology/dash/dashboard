<?php
/* Copyright (c) 2007 IBM, made available under EPL v1.0
 * Contributors Nick Boldt
 *
 * Web app for retrieving database schema information. For REST API (plain text) output, see ../web-api/schema.php
 */
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); $App = new App(); $Nav = new Nav(); $Menu = new Menu(); include($App->getProjectCommon());
ob_start();

$theme = "Phoenix";

ini_set('display_errors', 1); ini_set('error_reporting', E_ALL);

$pageTitle = "Project Dash - Dashboard Queries";

// name => (url, image, desc)
$dashboards = array(
	"Company &amp; Project Details" => array("commit-count-loc.php", "commit-count-loc.png", 
		"Explore commits, committers, and lines of code (LOC) by company, project, and committer. See ratio of active to inactive committers. 
		Compare committer activity as one measure of project health or company contribution."
	),
	"Project Diversity" => array("project-diversity.cgi", "project-diversity.png", 
		"Examine the committer diversity in the
		projects (or <a href=\"project-diversity.cgi?top=true\">top-level projects</a>) based on commits to the Eclipse source code repositories."
	),
	"Commits Explorer" => array("summary.cgi", "summary.png", 
		"Explore the \"commits to the source code (Git and SVN) repositories\" for all the
	 	Eclipse projects."
	),
	"Active Committers" => array("active-committers.cgi", "active-committers.png", 
		"Examine which committers are active in the Eclipse source code repositories."
	),
	"Active Projects" => array("active-projects.cgi", "active-projects.png",
		"Examine which projects (or <a href=\"active-projects.cgi?top=true\"><b>top-level projects</b></a>) are active in the Eclipse source code repositories.
		Also examine the <a href=\"active-projects-2.cgi\"><b>projects'</b></a> and <a href=\"active-projects-2.cgi?top=true\"><b>top projects'</b></a> activity 
		by the number of active committers."
	),
	"Database Schema" => array("schema.php", null, 
		"The database schema that backs all the above queries."
	),
	// ...
);

$APIs = loadDir("../web-api/", "commit-.*\.(php|cgi)", "f");
$APIs[] = "schema.php";
sort($APIs); reset($APIs);

print "<div id=\"midcolumn\">\n";

print "<h1>$pageTitle</h1>";

print "<p><i><b>
This automatically collected information may not represent true activity and should not be used as sole indicator of individual or project behavior.</b></i></p>\n";

print "<p><font color=\"red\" size=\"+2\">Please Note: this dashboard is deprecated, and will be shut down soon.</font> Please use the <a href=\"http://dashboard.eclipse.org\">new dashboard</a>.</p>";

foreach ($dashboards as $dashboard_name => $dashboard_data)
{
	print "<div class=\"homeitem\">\n";
	print "<h3><a name=\"" . str_replace(" ", "_", $dashboard_name) . "\"></a>" . $dashboard_name . "</h3>\n";
	print '<table cellpadding="15"><tr><td><a href="' . $dashboard_data[0] . '">' . ($dashboard_data[1] ? '<img src="thumbs/' . $dashboard_data[1] . '" width="200" border="0">' : $dashboard_name) . '</a></td>'."\n";
	print '<td><a href="' . $dashboard_data[0] . '"><b>' . $dashboard_name . '</b></a> - ' . $dashboard_data[2] . '</td></tr></table>'. "\n";
	print "</div>\n";
}

print "</div>\n"; // midcolumn

print "<div id=\"rightcolumn\">\n";

print "<div class=\"sideitem\">\n";
print "<h6>Dashboards</h6>\n";
print "<p><ul>\n";
foreach ($dashboards as $dashboard_name => $dashboard_data)
{
	print "<li><a href=\"" . $dashboard_data[0] . "\">$dashboard_name</a></li>\n";
}
print "</ul></p>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>REST APIs</h6>\n";
print "<p><ul>\n";
foreach ($APIs as $API)
{
	if (!preg_match("#\.inc#", $API))
	{
		print "<li><a href=\"../web-api/" . $API . "\">" . ucwords(str_replace("-", " ", preg_replace("#\.php$|\.cgi$|^commit-#", "", $API))) . "</a></li>\n";
	}
}
print "</ul></p>\n";
print "<p><ul>\n";
print "</ul></p>\n";
print "</div>\n";

print "<div class=\"sideitem\">\n";
print "<h6>Bugs &amp; Known Issues</h6>\n";
print "<p><ul>\n";
print "<li><a href=\"http://wiki.eclipse.org/Dash_Project/Commits_Explorer\">Known Issues</a></li>\n";
print "<li><a href=\"https://bugs.eclipse.org/bugs/enter_bug.cgi?product=dash&component=Commits+Explore&bug_severity=enhancement\">Request Enhancement</a></li>\n";
print "<li><a href=\"https://bugs.eclipse.org/bugs/enter_bug.cgi?product=dash&component=Commits+Explorer\">Report a Bug</a></li>\n";

print "</ul></p>\n";
print "</div>\n";

print "</div>\n"; // rightcolumn

print "</div>\n"; 

$html = ob_get_contents();
ob_end_clean();

$pageKeywords = ""; 
$pageAuthor = "Nick Boldt";

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);

function loadDir($dir, $ext, $type) // 1D array, not 2D
{
	$stuff = array();

	if (is_dir($dir) && is_readable($dir))
	{
		$handle = opendir($dir);
		while (($file = readdir($handle)) !== false)
		{
			if (preg_match("/$ext$/", $file) && !preg_match("/^\.{1,2}$/", $file))
			{
				if (($type == "d" && is_dir("$dir/$file")) || ($type == "f" && is_file("$dir/$file")))
				{
					$stuff[] = $file;
				}
			}
		}
		closedir($handle);
	}
	return $stuff;
}

?>
