<?php
/* Copyright (c) 2006, 2007 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt
 *
 * The REST web-api for retrieving records from the database.
 *
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 * 
 * fields=field1,field2,field3,... 
 *   where fields can be any of: ID, 
 *	   DATE, YEAR, YEARMONTH, YEARMONTHDAY,
 *	   TOPPROJECT, PROJECT,
 *	   FILENAME, FILETYPE,
 *	   REVISION, CHANGE_SIZE, MESSAGE_SIZE, 
 *	   LOGIN, COMPANY
 *
 * If fields is not set all of the above will be returned.  
 * 
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/commit-details.php?month=200711&login=bfreeman
 * http://dash.eclipse.org/dash/commits/web-api/commit-details.php?project=technology.dash&fields=company,login,project,yearmonthday,change_size&login=nickb&month=200711 
 *  
 */
header("Content-type: text/plain");
require_once "commit-common.inc.php";
$fields = getFields();
list( , , $where) = getXYWHERE();

displayQuery("SELECT $fields FROM commits" . ($where ? " WHERE " . $where : "") . " ORDER BY DATE LIMIT 1000");
?>
