<?php
/* Copyright (c) 2006, 2007 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt
 *
 * The REST web-api for retrieving records from the database.
 * 
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 * 
 * queries={count,detail}
 * fields=field1,field2,field3,... 
 *   where fields can be any of: ID, 
 *	   DATE, YEAR, YEARMONTH, YEARMONTHDAY,
 *	   TOPPROJECT, PROJECT,
 *	   FILENAME, FILETYPE,
 *	   REVISION, CHANGE_SIZE, MESSAGE_SIZE, 
 *	   LOGIN, COMPANY
 *
 * If fields is not set the default fields are DATE, CHANGE_SIZE, REVISION, FILENAME
 * 
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/commit-zero-length-changes.php?login=thabeck&project=eclipse.equinox&yearmonth=200707&queries=count
 *  
 */
header("Content-type: text/plain");
require_once "commit-common.inc.php";
$fields = getFields("DATE, CHANGE_SIZE, REVISION, FILENAME");
list( , , $where) = getXYWHERE();

if (!isset($_SERVER["QUERY_STRING"]) || !$_SERVER["QUERY_STRING"])
{
	print "# Must specify at least one of the following values: top=, project=, company=, login=, year=, ...\n";
 	print "# Example: http://dash.eclipse.org/dash/commits/web-api/commit-zero-length-changes.php?login=thabeck&project=eclipse.equinox&yearmonth=200707\n";
	exit;
}

$validQueries = array(
	"count" => "SELECT COUNT(CHANGE_SIZE) FROM commits" . ($where ? " WHERE " . $where : "") . " AND CHANGE_SIZE = 0",
	"detail" => "SELECT $fields FROM commits" . ($where ? " WHERE " . $where : "") . " HAVING CHANGE_SIZE = 0 ORDER BY $fields LIMIT 1000"
);

$queries = getQueries(array("count","detail")); 
if (in_array("all", $queries))
{
	foreach ($validQueries as $queryname => $query)
	{
		displayQuery($query);
	}
}
else
{
	foreach ($queries as $queryname)
	{
		if (isset($validQueries[$queryname]))
		{
			displayQuery($validQueries[$queryname]);
		}
	}
}
?>