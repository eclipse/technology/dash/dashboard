<?php
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/
header("Content-type: text/plain");

require_once dirname(__FILE__) . '/../common/common-functions.inc';
mustBeEclipseFoundationCaller();

if (!isset($_GET['repo'])) exit('# Path must be set.');

$repo = $_GET['repo']; 
if (!preg_match('/^\/gitroot\//', $repo)) exit('# Invalid repository');

$repo = realpath($repo);

if (!file_exists($repo)) exit('# Repository does not exist.');
if (!is_dir($repo)) exit('# Not a valid repository.');

require_once dirname(__FILE__) . '/../common/git-functions.inc';
require_once dirname(__FILE__) . '/../common/git-logcache.inc';

updateGitLogCache($repo);

$out = fopen("php://output", 'w');
$sql = "select ref, committer, author, authorName, size, comment, commitDate as date from gitlogcache where repo='$repo'";
$result = dashboard_sql($sql);
while ($row = mysql_fetch_assoc($result)) {
	$ref = $row['ref'];
	$committer = $row['committer'];
	$author = $row['author'];
	$authorName = $row['authorName'];
	$size = $row['size'];
	$comment = $row['comment'];
	$date = $row['date'];
	
	fputcsv($out, array($ref,$committer,$author,$authorName,$size,$comment, $date));
}
fclose($out);
?>