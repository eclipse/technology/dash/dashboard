<?php
/* Copyright (c) 2006, 2007 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt
 *
 * The REST web-api for retrieving project diversity summaries from the database.
 *
 */
header("Content-type: text/plain");

error_reporting(E_ALL ^ E_NOTICE);

$user="dashboard";
require_once( "dbpassword.php" );
$database="dashboard";
$_dbh = mysql_connect('dashdbhost',$user,$password);
mysql_select_db( $database, $_dbh );

$column = "PROJECT";
$where = "";
$top = $_GET['top'];
if( $top != "" ) {
  preg_match( "([A-Za-z._-]*)", $top, $matches );
  if( $top == "true" ) {
	$column = "TOPPROJECT";
  } else {
	if( $where != "" ) $where .= " AND ";
	$where .= "TOPPROJECT = '$matches[0]'";
  }
}
$columncomma = $column . ", ";

$all = $_GET['all'];
if( $all != "" ) {
  if( $all == "true" ) {
	$column = "";
	$columncomma = "";
  }
}

$_query2 = "select COMPANY, $columncomma YEAR, SUM(CHANGE_SIZE) AS SUM FROM commits";
if( $where ) {
  $_query2 .= " WHERE $where";
}
$_query2 .= " GROUP BY COMPANY, $columncomma YEAR";

echo( "# " . $_query2 . "\n" );
$result = mysql_query($_query2,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row['COMPANY'] . "\t";
    if( !( $all == "true" ) ) {
	    echo $row[$column] . "\t";
	}
    echo $row['YEAR'] . "\t" . $row['SUM'] . "\n";
  }
}

?>
