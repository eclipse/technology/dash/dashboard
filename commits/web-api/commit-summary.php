<?php
/* Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson
 *
 * The REST web-api for retrieving summaries from the database.
 *
 * top=x | y | name
 * project=x | y | name
 * year=x | y | yyyy
 * month=x | y | yyyymm
 * day=x | y | yyyymmdd
 * login=x | y | name
 * file=x | y | name
 * type=x | y | name
 * change=x | y | nnnn
 * message=x | y | nnnn
 * company=x | y | name
 * 
 * Exactly one "x" and one "y" and any number of other values.
 * 
 * Example:
 * http://dash.eclipse.org/dash/commits/web-api/commit-summary.php?company=x&login=y&project=tools.pdt&year=2007
 *  
 */
header("Content-type: text/plain");
require_once "commit-common.inc.php";
list($columnX, $columnY, $where) = getXYWHERE();

if($columnX && $columnY) {
	displayQuery("SELECT $columnX AS X, $columnY AS Y, COUNT(*) AS COUNT FROM commits" . ($where ? " WHERE " . $where : "") . " GROUP BY $columnX, $columnY");
}
else
{
	print "#\n # Error!\n# Must specify =x and =y values, such as http://dash.eclipse.org/dash/commits/web-api/commit-summary.php?company=x&login=y&project=tools.pdt&year=2007\n# \n";
}
?>