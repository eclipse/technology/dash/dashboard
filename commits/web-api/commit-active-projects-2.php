<?php
/* Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
 *
 * The REST web-api for retrieving active project summaries from the database.
 *
 */
header("Content-type: text/plain");

error_reporting(E_ALL ^ E_NOTICE);

$user="dashboard";
require_once( "dbpassword.php" );
$database="dashboard";
$_dbh = mysql_connect('dashdbhost',$user,$password);
mysql_select_db( $database, $_dbh );

$column = "PROJECT";
$where = "";
$top = $_GET['top'];
if( $top != "" ) {
  preg_match( "([A-Za-z._-]*)", $top, $matches );
  if( $top == "true" ) {
	$column = "TOPPROJECT";
  } else {
	if( $where != "" ) $where .= " AND ";
	$where .= "TOPPROJECT = '$matches[0]'";
  }
}
$company = $_GET['company'];
if( $company != "" ) {
  preg_match( "([A-Za-z._-]*)", $company, $matches );
  if( $where != "" ) $where .= " AND ";
  $where .= "COMPANY = '$matches[0]'";
}

$_query0 = "select MAX(YEARMONTH) AS MAX FROM commits";
$_query1 = "select MIN(YEARMONTH) AS MIN FROM commits";
if( $where ) {
  $_query1 .= " WHERE $where";
}
$_query2 = "select $column, YEARMONTH, COUNT(DISTINCT LOGIN) AS COUNT from commits";
if( $where ) {
  $_query2 .= " WHERE $where";
}
$_query2 .= " GROUP BY $column, YEARMONTH";

echo( "# " . $_query0 . "\n" );
$result = mysql_query($_query0,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row['MAX'] . "\n";
  }
}

echo( "# " . $_query1 . "\n" );
$result = mysql_query($_query1,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row['MIN'] . "\n";
  }
}

echo( "# " . $_query2 . "\n" );
$result = mysql_query($_query2,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row[$column] . "\t" . $row['YEARMONTH'] . "\t" . $row['COUNT'] . "\n";
  }
}

?>
