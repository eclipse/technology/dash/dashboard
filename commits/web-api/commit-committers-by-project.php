<?php
/* Copyright (c) 2008 IBM, made available under EPL v1.0
 * Contributors Nick Boldt
 *
 * The REST web-api for retrieving number of distinct committers per project, optionally filtered by company, project or date range
 *
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 * range={1day, 1wk, 1mo, 3mo, 6mo, 9mo, 12mo, 1yr, day, month, year}
 *
 * fields=field1,field2,field3 where fieldN is one of top, project, year, ...
 *
 * Defaults:
 * If not specified, fields will be set to PROJECT.
 *
 * If not specified, range will default to year.
 * If not specified, year will be set to the current year.
 *
 * If one of ?day=, ?month=, or ?year= are specified, range will be set to that value (eg., ?day=20070702 equals ?range=day&day=20070702).
 * If set range is not equal to a specified day, month, or year, range will be respected first (eg., ?range=month&year=2007 equals ?range=month (current month)).
 * If one of range={day,month,year} is set but no day, month, or year are specified, value will be current day, month or year (eg., ?range=year equals ?year=2008 while the current year is 2008).
 *
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/commit-committers-by-project.php?fields=project&year=2008&company=Zend%25
 * http://dash.eclipse.org/dash/commits/web-api/commit-committers-by-project.php?fields=project&year=2008&company=IBM&project=modeling%25
 * 
 * See also:
 * http://dash.eclipse.org/dash/commits/web-api/commits-index.php?projectid=technology.dash
 * http://dash.eclipse.org/dash/commits/web-api/schema.php
 *
 */
header("Content-type: text/plain");

require_once "commit-common.inc.php";
$debug = isset($_GET['debug']); 

$counts = array();
$countsTop = array();

$query = "SELECT DISTINCT TOPPROJECT, PROJECT from commits";
$data = $debug ? displayQueryAndReturn($query) : runQuery($query);
foreach ($data as $row) // $row[0] = TOPPPROJECT, $row[1] = PROJECT
{
	// get count of commmitters for each project
	$query = "SELECT DISTINCT LOGIN FROM commits_index WHERE PROJECT='" . $row[1] . "'";
	$res = $debug ? displayQueryAndReturn($query) : runQuery($query);
	foreach ($res as $row2)
	{
		// combine results as list of LOGINS, indexed by TOPPROJECT
		if (!isset($countsTop[$row[0]]))
		{
			$countsTop[$row[0]] = array();
		}
		$countsTop[$row[0]][$row2[0]] = $row2[0];

		// combine results as list of LOGINS, indexed by PROJECT
		if (!isset($counts[$row[1]]))
		{
			$counts[$row[1]] = array();
		}
		$counts[$row[1]][$row2[0]] = $row2[0];
	} 
}

print $debug ? "\n" : ""; 

print "# TOPPROJECT\tCOUNT(DISTINCT LOGIN)\tLOGINS\n";
ksort($countsTop); reset($countsTop);
foreach ($countsTop as $projname => $logins)
{
	print "$projname\t" . sizeof($logins) . "\t";
	sort($logins); reset($logins);
	foreach ($logins as $login)
	{
		print $login . "\t";
	}
	print "\n";
}
print "\n";

print "# PROJECT\tCOUNT(DISTINCT LOGIN)\tLOGINS\n";
ksort($counts); reset($counts);
foreach ($counts as $projname => $logins)
{
	print "$projname\t" . sizeof($logins) . "\t";
	sort($logins); reset($logins);
	foreach ($logins as $login)
	{
		print $login . "\t";
	}
	print "\n";
}
print "\n"; ?>