<?php
/* Copyright (c) 2007 IBM, made available under EPL v1.0
 * Contributors Nick Boldt
 *
 * The REST web-api for retrieving number & size (LOC) of commits per committer/project/company/....
 * For formatted HTML output, see ../web-app/commit-count-loc.php
 *
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 * range={1day, 1wk, 1mo, 3mo, 6mo, 9mo, 12mo, 1yr, day, month, year}
 *
 * fields=field1,field2,field3 where fieldN is one of top, project, year, ...
 *
 * Defaults:
 * If not specified, fields will be set to COMPANY, PROJECT, LOGIN.
 *
 * If not specified, range will default to year.
 * If not specified, year will be set to the current year.
 *
 * If one of ?day=, ?month=, or ?year= are specified, range will be set to that value (eg., ?day=20070702 equals ?range=day&day=20070702).
 * If set range is not equal to a specified day, month, or year, range will be respected first (eg., ?range=month&year=2007 equals ?range=month (current month)).
 * If one of range={day,month,year} is set but no day, month, or year are specified, value will be current day, month or year (eg., ?range=year equals ?year=2008 while the current year is 2008).
 *
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/commit-count-loc.php?fields=company,project,login&year=2007&company=Zend%25
 * http://dash.eclipse.org/dash/commits/web-api/commit-count-loc.php?fields=company,project,login&year=2007&company=IBM&project=modeling%25
 *
 */
header("Content-type: text/plain");

require_once "commit-common.inc.php";
$fields = getFields("COMPANY, PROJECT, LOGIN");

$YMD = getYMD();
$validRanges = getValidRanges("Ymd", $YMD);
$range = getRange(isset($_GET["range"]) ? $_GET["range"] : "year", "year", false); // defaults to year range
print $debug ? "\$range = $range<br/>\n" : ""; unset($_GET["range"]);
$rangeSQL = getRange($range, "YEAR = " . date("Y"), true); print $debug ? "\$rangeSQL = $rangeSQL<br/>\n" : "";
list( , , $where) = getXYWHERE(); print $debug ? "\$where = $where<br/>\n" : "";

$query = "SELECT " . ($fields ? $fields . ", " : "") . "COUNT(CHANGE_SIZE) AS NUM_COMMITS, SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE " .
	($where ? $where . " AND " : "") . $rangeSQL . // this query should always have a $rangeSQL clause
	($fields ? " GROUP BY $fields" : "");

displayQuery($query); ?>