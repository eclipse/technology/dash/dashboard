<?php
/* Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
 *
 * The REST web-api for retrieving active committer summaries from the database.
 *
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * login=name
 * file=name
 * type=name
 * change=nnnn
 * message=nnnn
 * company=name
 *
 * queries={logins,1day,1wk,1mo,3mo,6mo,9mo,12mo,year,all}
 * fields=field1,field2,field3 where fieldN is one of top, project, year, ...
 *
 * Defaults:
 * At least one WHERE clause constaint must be set (or query will time out); error message is shown instead to minimize server churn.
 * If no value of queries is set, 5 queries will be returned: logins,1mo,3mo,6mo,9mo
 * If not specified, year (in the "year" query) will be set to the current year.
 * If not specified, fields will be set to LOGIN.
 *
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/commit-active-committers.php?company=BEA
 * http://dash.eclipse.org/dash/commits/web-api/commit-active-committers.php?project=technology.dash
 * http://dash.eclipse.org/dash/commits/web-api/commit-active-committers.php?company=Zend%25&queries=logins,year
 *
 */
header("Content-type: text/plain");
require_once "commit-common.inc.php";
$fields = getFields("LOGIN");
$year = isset($_GET["year"]) ? $_GET["year"] : date("Y");
list( , , $where) = getXYWHERE();

if (!isset($_SERVER["QUERY_STRING"]) || !$_SERVER["QUERY_STRING"])
{
	print "# Must specify at least one of the following values: top=, project=, company=, queries=, fields=, ...\n";
	print "# Example: http://dash.eclipse.org/dash/commits/web-api/commit-active-committers.php?project=technology.dash\n";
	print "# Example: http://dash.eclipse.org/dash/commits/web-api/commit-active-committers.php?company=Zend%25&queries=logins,year\n";
	exit;
}

$fmt = "Ymd";
$validQueries = array(
	"logins"     => "select DISTINCT " . ($fields ? $fields : "") . " FROM commits" . ($where ? " WHERE " . $where : "") . " ORDER BY LOGIN",

	"1day"=> "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-1 day"))   . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"1wk" => "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-1 week"))  . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"1mo" => "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-1 month")) . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"3mo" => "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-3 month")) . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"6mo" => "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-6 month")) . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"9mo" => "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-9 month")) . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
	"12mo"=> "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEARMONTHDAY >= " . date($fmt, strtotime("-12 month")). ($where ? " AND " . $where : "") . " GROUP BY LOGIN",

	"year"=> "select " . ($fields ? $fields . ", " : "") . " SUM(CHANGE_SIZE) AS NUM_LOC FROM commits WHERE YEAR = " . $year . ($where ? " AND " . $where : "") . " GROUP BY LOGIN",
);

$queries = getQueries(array("logins", "1mo", "3mo", "6mo", "9mo"));
if (in_array("all", $queries))
{
	foreach ($validQueries as $queryname => $query)
	{
		displayQuery($query);
	}
}
else
{
	foreach ($queries as $queryname)
	{
		if (isset($validQueries[$queryname]))
		{
			displayQuery($validQueries[$queryname]);
		}
	}
}
?>