<?php
/* Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt, Karl Matthias
 *
 * The common parameter parsing module for the REST web-api
 * for retrieving data from the database. This is NOT part
 * of the public web-api.
 *
 * top=x | y | name
 * project=x | y | name
 * year=x | y | yyyy
 * month=x | y | yyyymm
 * day=x | y | yyyymmdd
 * login=x | y | name
 * file=x | y | name
 * type=x | y | name
 * change=x | y | nnnn
 * message=x | y | nnnn
 * company=x | y | name
 *
 * Exactly one "x" and one "y" and any number of other values.
 */
ini_set('display_errors', 1); ini_set('error_reporting', E_ALL);
$debug = isset($_GET["debug"]) ? $_GET["debug"] : 0;

$user="dashboard";
require_once("dbpassword.php");
$database="dashboard";
if (isset($password) && $password)
{
	$_dbh = mysql_connect('dashdbhost', $user, $password);
	mysql_select_db($database, $_dbh);
}
else
{
	$_dbh = null;
}

/* Get X, Y, where as returned array of variables. Assign with list($columnX, $columnY, $where) = getXYWHERE(); */
function getXYWHERE()
{
	global $debug;
	$param = null;
	$columnX = "";
	$columnY = "";
	$where = "";

	$input_patterns = array(
		/* regex => array(fieldname => http get variable) */
		"#([0-9%]+)#" => array(
			"CHANGE_SIZE" => "change",
			"MESSAGE_SIZE" => "message",
			"YEAR" => "year",
			"YEARMONTH" => "month",
			"YEARMONTHDAY" => "day",
		),
		"#([a-zA-Z%_\.-]+)#" => array(
			"LOGIN" => "login",
			"TOPPROJECT" => "top",
			"FILETYPE" => "type",
		),
		"#([a-zA-Z0-9%_\.-]+)#" => array(
			"PROJECT" => "project",
		),
		"#([a-zA-Z% ,\.-]+)#" => array(
			"COMPANY" => "company",
		),
		"#([a-zA-Z0-9% _\/\$\.-]+)#" => array(
			"FILENAME" => "file",
		)
	);

	foreach($input_patterns as $regex => $pairs)
	{
		foreach ($pairs as $sqlfieldname => $httpfieldname)
		{
			$param = isset($_GET[$httpfieldname]) ? $_GET[$httpfieldname] : (isset($_GET[$sqlfieldname]) ? $_GET[$sqlfieldname] : null);
			if ($debug)
			{
				print "<pre>     [$regex][$sqlfieldname][$httpfieldname] Got \$param = $param     </pre>\n";
			}
			if ($param && strtolower($param) == "x")
			{
				 $columnX = $sqlfieldname; /* use as X column */
			}
			else if ($param && strtolower($param) == "y")
			{
				 $columnY = $sqlfieldname; /* use as Y column */
			}
			else if ($param)
			{
				if (preg_match($regex, $param, $matches))
				{
		  			$where .= ($where != "" ? " AND " : "") . $sqlfieldname . " LIKE " .
		  				(is_numeric($matches[1]) ? $matches[1] : "'" . $matches[1] . "'"); /* add to WHERE clause */
		  		}
			}
		}
	}
	return array($columnX, $columnY, $where);
}

/* get user-specified fields as CSV string, or fall back to a default list */
function getFields($defaultFields = "")
{
	$fieldsOut = "";
	$validFields = array("ID", "DATE", "YEAR", "YEARMONTH", "YEARMONTHDAY", "TOPPROJECT", "PROJECT", "FILENAME", "FILETYPE", "REVISION", "CHANGE_SIZE", "MESSAGE_SIZE", "LOGIN", "COMPANY");
	if (isset($_GET["fields"]) && preg_match("#([a-zA-Z,_ ]+)#", $_GET["fields"], $matches))
	{
		$bits = preg_split("#[, ]+#", $matches[1]);
		foreach ($bits as $bit)
		{
			if (in_array(strtoupper($bit), $validFields))
			{
				$fieldsOut .= ($fieldsOut != "" ? ", " : "") . strtoupper($bit); // if valid field name, collect
			}
		}
	}

	// support $defaultFields input as array or CSV string
	$defaultFields = $defaultFields && is_array($defaultFields) && sizeof($defaultFields) > 1 ? implode(", ", $defaultFields) : $defaultFields;

	// return the input collected values; or, the function's default list; or, the entire list if no default given
	return $fieldsOut ? $fieldsOut : ($defaultFields ? $defaultFields : implode(", ", $validFields));
}


// for convenience
function displayQueryAndReturn($_query)
{
	return displayQuery($_query, true);
}

// to return an array of queried data, second param true
function displayQuery($_query, $returnArray = false)
{
	global $_dbh;
	echo( "# " . $_query . "\n" );
	$result = mysql_query($_query,$_dbh);
	$data = array();
	if (!$result) {
		echo("# MySQL Error: ".mysql_error() . "\n");
	} else {
		while($row = mysql_fetch_array($result)){
			if ($returnArray)
			{
				$data[] = $row;
			}
			for ($i=0; $i<sizeof($row); $i++)
			{
				print isset($row[$i]) ? $row[$i] . "\t" : "";
			}
			print "\n";
		}
	}
	return $returnArray ? $data : null;
	# Don't put a blank line here.  Other code parses this.  Thanks.
}

// to return an array of queried data w/o displaying output
function runQuery($_query)
{
	global $_dbh;
	$result = mysql_query($_query,$_dbh);
	$data = array();
	if (!$result) {
		echo("# MySQL Error: ".mysql_error() . "\n");
	} else {
		while($row = mysql_fetch_array($result)){
			$data[] = $row;
		}
	}
	return $data;
}

/* get user-specified list of queries to run as an array of query names, or fall back to a default list */
function getQueries($defaultQueries = array()) // default queries to show if none requested
{
	global $validQueries;
	$queriesOut = array();
	$validQueryNames = array_keys($validQueries); $validQueryNames[] = "all";
	if (isset($_GET["queries"]) && preg_match("#([a-zA-Z0-9,_ ]+)#", $_GET["queries"], $matches))
	{
		$bits = preg_split("#[, ]+#", $matches[1]);
		foreach ($bits as $bit)
		{
			if (in_array(strtolower($bit), $validQueryNames))
			{
				$queriesOut[] = strtolower($bit); // if valid query name, collect
			}
		}
	}

	// return the input collected values; or, the function's default list; or, the entire list if no default given
	return $queriesOut ? $queriesOut : ($defaultQueries && is_array($defaultQueries) && sizeof($defaultQueries) > 0 ? $defaultQueries : $validQueryNames);
}

/* return array of lines from a given host and URL */
function http_file($host, $url, $qs=null)
{
	if (ini_get('allow_url_fopen'))
	{
		return file("http://" . $host . (isset($url) ? $url : '/') . (isset($qs) ? '?' . $qs : ''));
	}
	else
	{
		return preg_split("#(\n\r|\r\n|\n|\r)#", http_file_get_contents($host, $url));
	}
}

/* return a block of text (including newlines) from a given host and URL */
function http_file_get_contents($host, $url, $qs=null)
{
	if (ini_get('allow_url_fopen'))
	{
		return file_get_contents("http://" . $host . (isset($url) ? $url : '/') . (isset($qs) ? '?' . $qs : ''));
	}
	else
	{
		$file_contents = "";
		$fp = fsockopen($host, 80, $errno, $errstr, 30);
		if (!$fp)
		{
			return "ERROR connecting to $host: $errstr ($errno)";
		}
		else
		{
			fputs($fp, "GET " . (isset($url) ? $url : '/') . (isset($qs) ? '?' . $qs : '') . " HTTP/1.1\r\n");
			fputs($fp, "Host: $host\r\nUser-Agent: PHP Script\r\nContent-Type: application/x-www-form-urlencoded\r\nConnection: close\r\n\r\n");
			while (!feof($fp))
			{
				$file_contents .= fgets($fp, 128);
			}
			fclose($fp);
		}
		return $file_contents;
	}
}

/* get year, month, day values, and unset their $_GET variables */
function getYMD()
{
	global $debug, $_GET;
	$year = isset($_GET["year"]) ? $_GET["year"] : null;	print $debug ? "\$year = $year<br/>\n" : "";	unset($_GET["year"]);
	$month = isset($_GET["month"]) ? $_GET["month"] : null;	print $debug ? "\$month = $month<br/>\n" : "";	unset($_GET["month"]);
	$day = isset($_GET["day"]) ? $_GET["day"] : null;		print $debug ? "\$day = $day<br/>\n" : "";		unset($_GET["day"]);

	// override range with year, month, or day
	if (!isset($_GET["range"]) || !$_GET["range"])
	{
		if ($year)
		{
			$_GET["range"] = "year";
		}
		else if ($month)
		{
			$_GET["range"] = "month";
		}
		else if ($day)
		{
			$_GET["range"] = "day";
		}
	}
	else
	{
		if ($_GET["range"] == "year" && !$year)
		{
			$year = date("Y");
		}
		else if ($_GET["range"] == "month" && !$month)
		{
			$month = date("Ym");
		}
		else if ($_GET["range"] == "day" && !$day)
		{
			$day = date("Ymd");
		}
	}
	return array($year, $month, $day);
}

function getValidRanges($fmt = "Ymd", &$YMD)
{
	return array(
		"1day"=> "YEARMONTHDAY >= " . date($fmt, strtotime("-1 day"))   ,
		"1wk" => "YEARMONTHDAY >= " . date($fmt, strtotime("-1 week"))  ,
		"1mo" => "YEARMONTHDAY >= " . date($fmt, strtotime("-1 month")) ,
		"3mo" => "YEARMONTHDAY >= " . date($fmt, strtotime("-3 month")) ,
		"6mo" => "YEARMONTHDAY >= " . date($fmt, strtotime("-6 month")) ,
		"9mo" => "YEARMONTHDAY >= " . date($fmt, strtotime("-9 month")) ,
		"12mo"=> "YEARMONTHDAY >= " . date($fmt, strtotime("-12 month")),
		"1yr" => "YEARMONTHDAY >= " . date($fmt, strtotime("-12 month")),

		"day"  => "YEARMONTHDAY = " . ($YMD[2] ? $YMD[2] : date("Ymd")),
		"month"=> "YEARMONTH = "    . ($YMD[1] ? $YMD[1] : date("Ym")),
		"year" => "YEAR = "         . ($YMD[0] ? $YMD[0] : date("Y")),
	);
}

function getRange($key, $def, $returnSQL = true)
{
	global $validRanges;
	return isset($key) && $key && array_key_exists($key, $validRanges) ? ($returnSQL ? $validRanges[$key] : $key) : $def;
}

?>
