<?php
/* Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson
 *
 * The REST web-api for retrieving lists of projects from the database.
 *
 */
header("Content-type: text/plain");

error_reporting(E_ALL ^ E_NOTICE);

$user="dashboard";
require_once( "dbpassword.php" );
$database="dashboard";
$_dbh = mysql_connect('dashdbhost',$user,$password);
mysql_select_db( $database, $_dbh );

$_query1 = "select distinct PROJECT from commits order by PROJECT";
$_query2 = "select distinct TOPPROJECT from commits order by TOPPROJECT";

echo( "# " . $_query1 . "\n" );
$result = mysql_query($_query1,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row['PROJECT'] . "\n";
  }
}

echo( "# " . $_query2 . "\n" );
$result = mysql_query($_query2,$_dbh);
if (!$result) {
  echo("MySQL Error: ".mysql_error());
} else {
  while($row = mysql_fetch_assoc($result)){
    echo $row['TOPPROJECT'] . "\n";
  }
}

?>
