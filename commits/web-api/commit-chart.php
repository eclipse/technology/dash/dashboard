<?php
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

$id = getParameter('id', '^[a-zA-Z0-9\-]+(\.[a-zA-Z0-9\-]+){0,2}$');
$width = getIntParameter('width', 800);
$height = getIntParameter('height', 100);

getData($id, $data, $minPeriod, $maxPeriod, $minValue, $maxValue);

$keys = array();
$count = 3;
$labelFrequency = floor(count($data) / ($width / 120));
foreach ($data as $key=>$value) {	
	preg_match('/(\d\d\d\d)(\d\d)/', $key, $matches);
	$year = $matches[1];
	$month = $matches[2];
	$keys[] = $count++ % $labelFrequency == 0 ? date("M%20Y", strtotime("$year-$month-01")) : '';
}

//ksort($data);

$data = join(',', $data);
$keys = join('|', $keys);

$magnitude = pow(10, floor(log($maxValue, 10)));
$max = ceil($maxValue / $magnitude) * $magnitude;

$dimensions = $width . 'x' . $height;
$url = "http://chart.apis.google.com/chart
   ?chxl=1:|$keys
   &chxr=0,0,$max
   &chxt=y,x
   &chbh=a,5
   &chs=$dimensions
   &cht=bvg
   &chco=A2C180
   &chds=0,$max
   &chd=t:$data";

$url = preg_replace('/\s+/', '', $url);

header("Content-type: image/png");
echo file_get_contents($url);

//header("Content-type: text/plain");
//echo $url;

function getParameter($name, $pattern, $default = null) {
	if (!isset($_GET[$name])) return $default;
	$value = $_GET[$name];
	if (preg_match("/$pattern/", $value, $matches)) return $matches[0];
	return $default;
} 

function getIntParameter($name, $default) {
	return getParameter($name, '\d+', $default);
}

function getData($id, &$data, &$minPeriod, &$maxPeriod, &$minValue, &$maxValue) {
	if (!getRealData($id, $data, $minPeriod, $maxPeriod, $minValue, $maxValue))
		getDummyData($data, $minPeriod, $maxPeriod, $minValue, $maxValue);
	
	preg_match('/(\d\d\d\d)(\d\d)/', $minPeriod, $matches);
	$year = $matches[1];
	$month = $matches[2];
	
	while (true) {
		$period = (int)sprintf('%04d%02d', $year, $month);
		if (!isset($data[$period])) $data[$period] = 0;
		if ($period == $maxPeriod) break;
		
		if ($month++ == 12) {
			$year++;
			$month = 1;
		}
	}
}
	
function getRealData($id, &$data, &$minPeriod, &$maxPeriod, &$minValue, &$maxValue) {
	$user="dashboard";
	require_once( "dbpassword.php" );
	$database="dashboard";
	$_dbh = mysql_connect('dashdbhost',$user,$password);
	if (!$_dbh) return false;
	
	mysql_select_db( $database, $_dbh );
	
	$where = $id ? " where project='$id' " : '';
	$sql = "select yearmonth as period, count(distinct revision) as count from commits $where group by yearmonth order by yearmonth";
	$result = mysql_query($sql);
	
	$data = array();
	while ($row = mysql_fetch_assoc($result)) {
		$period = (int)$row['period'];
		$minPeriod = $minPeriod ? min($minPeriod, $period) : $period;
		$maxPeriod = $maxPeriod ? max($maxPeriod, $period) : $period;
		
		$count = (int)$row['count'];
		$minValue = $minValue ? min($minValue, $count) : $count;
		$maxValue = $maxValue ? max($maxValue, $count) : $count;
		
		$data[$period] = $count;
	}
	return true;
}

function getDummyData(&$data, &$minPeriod, &$maxPeriod, &$minValue, &$maxValue) {
	
	$sampledata="
	|    200805 |            5740 | 
	|    200806 |            4395 | 
	|    200807 |            2400 | 
	|    200808 |             445 | 
	|    200809 |             837 | 
	|    200810 |            1447 | 
	|    200811 |            1081 | 
	|    200812 |            2355 | 
	|    200901 |            7013 | 
	|    200902 |           17599 | 
	|    200903 |            6900 | 
	|    200904 |            1916 | 
	|    200905 |            8801 | 
	|    200906 |            2565 | 
	|    200907 |            2127 | 
	|    200911 |            1515 | 
	|    200912 |            2186 | 
	|    201001 |            3391 | 
	|    201002 |           13543 | 
	|    201003 |            2498 | 
	|    201004 |            4470 | 
	|    201005 |            5495 | 
	|    201006 |             613 | 
	|    201007 |             629 | 
	|    201008 |            2778 | 
	|    201009 |            2281 | 
	|    201010 |            4633 | 
	|    201011 |            3979 | 
	|    201101 |            3207 | 
	|    201102 |            2923 | 
	|    201103 |            1704 | 
	|    201104 |             707 | 
	";
	
	$data = array();
	$lines = split("\n", $sampledata);
	foreach($lines as $line) {
		if (preg_match('/[\s]*([\d]+) \|[\s]*([\d]+)/', $line, $matches)) {
			$period = (int)$matches[1];
			$minPeriod = $minPeriod ? min($minPeriod, $period) : $period;
			$maxPeriod = $maxPeriod ? max($maxPeriod, $period) : $period;
			
			$count = (int)$matches[2];
			$minValue = $minValue ? min($minValue, $count) : $count;
			$maxValue = $maxValue ? max($maxValue, $count) : $count;
			
			$data[$period] = $count;
		}
	}

}

?>