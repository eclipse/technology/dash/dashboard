<?php
/* Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias, Nick Boldt
 *
 */
$password_file = "/var/lib/dashboard/DBPASSWORD.txt";
$password = is_file($password_file) && is_readable($password_file) ? rtrim(file_get_contents($password_file)) : null;
?>
