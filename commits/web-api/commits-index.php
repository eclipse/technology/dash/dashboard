<?php
/* Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt, Karl Matthias
 *
 * The REST web-api for retrieving records from the database.
 *
 * Return data from commits_index table
 *  
 */
header("Content-type: text/plain");
require_once "commit-common.inc.php";

if( !isset($_REQUEST['projectid'])
 || preg_match('/^([a-z.0-9\-_,]+)$/', $_REQUEST['projectid'], $matches) === FALSE ) {
	displayQuery("SELECT LOGIN, COUNT, PERIOD, PROJECT FROM commits_index ORDER BY COUNT DESC");
} else {
	$projectid = $matches[1];
	$projectids = explode( ',', $projectid );
	displayQuery("SELECT LOGIN, COUNT, PERIOD FROM commits_index WHERE PROJECT IN ('" .
		implode( "','", $projectids ) . "') ORDER BY COUNT DESC");
}
?>
