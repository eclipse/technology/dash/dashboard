#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2006, 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Ward Cunningham, Bjorn Freeman-Benson (Eclipse Foundation)
# *		- initial API and implementation
# *    Karl Matthias (Eclipse Foundation)
# *    Wayne Beaton (Eclipse Foundation) - Bug 351584
# *******************************************************************************/
#
# usage:

./get-companies.pl
./get-roots.pl

# CVS
touch start.time
cat roots-cvs.txt | ./extract.pl | sed 's/author: ibull/author: irbull/' | ./parse.pl | ./chunk.pl insert.pl
./rewrite-history.pl < rewrite-history.txt
mv start.time last.time
touch finish.time

# SVN
cat roots-svn.txt | ./svn-extract.pl | ./svn-parse.pl | ./chunk.pl insert.pl
# last.revisions is maintained by svn-parse.pl

# INDEX
./create-commits-index-table.pl
echo done
