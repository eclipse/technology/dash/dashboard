<?php
/*******************************************************************************
* Copyright (c) 2012 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Wayne Beaton (Eclipse Foundation)- initial API and implementation
*******************************************************************************/

/*
 * Some invalid entries have crept into the commit record due
 * to some LDAP issues. And Ian Bull being very particular about
 * his committer id.
 * 
 * This file generates SQL to fix the errors.
 */
$map = array(
	'ibull' => 'irbull',
	'uid8185' => 'gliu',
	'uid8428' => 'bvosburgh',
	'uid8436' => 'ydoshiro',
	'uid8762' => 'aigdalov',
	'uid8825' => 'dwagelaar',
	'uid8941' => 'mkhouzam',
	'uid9069' => 'pschonbac',
	'uid9273' => 'szarnekow',
	'uid9453' => 'sstundzig'
);

foreach($map as $bogus => $correct) {
	echo "update commits set login='$correct' where login='$bogus';\n";
}

?>