#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2006, 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Ward Cunningham, Bjorn Freeman-Benson (Eclipse Foundation)
# *		- initial API and implementation
# *    Karl Matthias (Eclipse Foundation)
# *    Wayne Beaton (Eclipse Foundation) - Bug 334531, Bug 351584
# *******************************************************************************/

# usage:

echo 'Starting Dash batch ' `date`
./get-companies.pl
./get-roots.pl

# prep for full batch
./create-batch-db.pl
rm words/*
rm -f last.time

# CVS is no longer supported.
# continue similar to incremental
#touch start.time
#cat roots-cvs.txt | ./extract.pl --ignoretime | sed 's/author: ibull/author: irbull/' | ./parse.pl | ./chunk.pl insert-batch.pl
#mv start.time last.time
#touch finish.time
# svn
echo '- SVN ' `date`
# HACK: We only have one repo in SVN. Don't be any more clever than necessary.
echo -e "technology.subversive\t/svnroot/technology/org.eclipse.subversive" | ./svn-extract.pl --ignoretime | ./svn-parse.pl | ./chunk.pl ./insert-batch.pl

# git
echo '- Git ' `date`
php ./git-roots.php | php ./git-extract.php | php ./git-parse.php | ./chunk.pl ./insert-batch.pl

# Scan clones of repositories on GitHub
find /home/data/gitmirrors/github -name *.git | awk -F/ '{printf("%s\t%s\n", $6,$0)}' | php ./git-extract.php | php ./git-parse.php | ./chunk.pl ./insert-batch.pl

# Ensure that the alternative forge repositories are up-to-date
# HACK: these clones are manually created in Wayne's user directory on build. Ugly, but temporary
find /home/data/users/wbeaton/dash/ -name *.git | awk -F/ '{print($0)}' | xargs -I {} git --git-dir={} fetch &> /dev/null
find /home/data/users/wbeaton/dash/ -name *.git | awk -F/ '{printf("%s\t%s\n", $7,$0)}' |php ./git-extract.php | php ./git-parse.php | ./chunk.pl ./insert-batch.pl

echo '- Renaming ' `date`
# finish up after the full batch
./rename-batch-db.pl

echo '- Generating commits index table ' `date`
# INDEX
./create-commits-index-table.pl

echo '- Rebuilding the query tables ' `date`
# Rebuild the query tables.
/usr/bin/mysql --user=dashboard --password=`cat dbpass` dashboard < create_query_tables.sql 

echo '-Done!' `date`
