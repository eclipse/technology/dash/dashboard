#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# insert-batch.pl  <  TAB_FILE
#

use strict;

exec "perl insert.pl --batch";
