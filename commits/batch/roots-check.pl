#!/usr/bin/perl
# Copyright (c) 2008 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
#
# usage:
# ./roots-check.pl

use strict;
use FileHandle;

my %paths;
my %cvsroots;
my %ignore;
my $line;
my $key;
my $path;
my $root;

print << 'EOF';
Please update the CVS/SVN project info meta-data about your project
using the portal (http://portal.eclipse.org/).
The weekly CVS/SVN activity monitor batch job has detected the following
errors in the meta-data, some of which are related to your project.
The project page activity graph, the active/inactive committer list,
the commits explorer, and other similar pages are all driven by this
meta-data.  Incorrect meta-data causes incorrectly low activity to be
reported for your project. Get credit for the hard work you are doing:
update your project's meta-data today.

EOF

for (</cvsroot/*/*>) {
	next if /CVSROOT/;
	$paths{$_} = 1;
}

for (</cvsroot/org.eclipse/www/*>) {
	next if /CVSROOT/;
	$paths{$_} = 1;
}

# Load up the roots-cvs.txt file
my $fh = new FileHandle("<roots-cvs.txt") or print "Error: can't open roots-cvs.txt";
while($line = $fh->getline()) {
	my @fields = split(/\s+/, $line);

	# Catch double-claimed ownership
	if(defined($cvsroots{$fields[1]})) {
		print " * $fields[0] is claiming ownership of $fields[1] which is also claimed by $cvsroots{$fields[1]}.\n";
	} else {
		$cvsroots{$fields[1]} = $fields[0];
	}
}
$fh->close();

# Load up the roots-ignore.txt file
my $fh = new FileHandle("<roots-ignore.txt") or print "Error: can't open roots-ignore.txt";
while($line = $fh->getline()) {
	my @fields = split(/\s+/, $line);
	$ignore{$fields[1]} = $fields[0];
}
$fh->close();

# Catch double entries
foreach $key (keys(%ignore)) {
	if(defined($cvsroots{$key})) {
		print " * $key exists in both roots-ignore.txt and roots-cvs.txt.\n";
	}
}

# Catch problems where projects claim non-existant paths
foreach $root (keys(%cvsroots)) {
	if(!defined($paths{$root})) {
		print " * $root is claimed by $cvsroots{$root} but does not exist.\n";
	}
}

# Catch non-claimed paths
foreach $path (keys(%paths)) {
	if(!defined($cvsroots{$path}) and !defined($ignore{$path})) {
		print " * $path is not claimed.\n";
	}
}


print << 'EOF1';

P.S. The committer activity data is incrementally updated over a seven day
period, so it might be a few days before your changes become visible.
EOF1
