#!/usr/bin/php
<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

/**
 * This script extracts data out of Git to feed into the ./git-parse.php script.
 * 
 * Usage:
 * 
 *  echo -e "technology.dash.woolsey\t/gitroot/woolsey/org.eclipse.woolsey.iplog.git" | ./git-extract.php
 *  
 * Examples:
 * 
 *  echo -e "technology.dash.woolsey\t/gitroot/woolsey/org.eclipse.woolsey.iplog.git\nrt.ecf\t/gitroot/ecf/org.eclipse.ecf.git\n" | ./git-extract.php 
 * @author Wayne Beaton
 */

while (!feof(STDIN)) {
	$line = fgets(STDIN);
	if (!trim($line)) continue;
	
	$parts = split("\t", $line);

	$projectid = trim($parts[0]);
	$path = trim($parts[1]);	
	
	// Try our best to extact the data. Some projects don't provide
	// this in the exact right manner. Skip it if we can't sort out
	// what they mean.
	// TODO Reinstate this code once we have access to /svnroot on Pebbles.
//	if (!preg_match('/^.*(\/gitroot\/.*)$/', $path, $matches)) continue;
//	$path = $matches[1];
	
	echo "Repository: $path\n";
	
	if (!preg_match('/\.git\/?$/', $path)) $path .= '/.git';
	
	echo "Project: $projectid\n";	
	system("/usr/local/bin/git --git-dir=$path log --branches --numstat --format=fuller");
	echo "\n\n";
}

?>
