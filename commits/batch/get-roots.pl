#!/usr/bin/perl
# Copyright (c) 2008 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
#
# usage:
# get-roots.pl
#
# writes:
# roots.txt
# roots-svn.txt

use strict;
use LWP;
use FileHandle;

my $them = "https://www.eclipse.org";
my $file = "/projects/web-api/repos.php";

sub get_data {
	my ($type) = @_;

	# Get content from web-api
	my $browser = LWP::UserAgent->new;
	# CVS
	my $result = $browser->get($them . $file . "?type=$type") or
		die "Can't open connection to $them";
	
	# Make sure we got something
	if(length($result->decoded_content()) < 5) {
		die "Error, unexpectedly short $type roots output: " . $result->decoded_content();
	}
	
	# Write output to file
	my $roots_file = new FileHandle(">roots-$type.txt") or die "Can't write to roots-$type.txt";
	$roots_file->print($result->decoded_content());
	$roots_file->close();
}

# CVS is no longer supported
#get_data('cvs');
get_data('svn');
