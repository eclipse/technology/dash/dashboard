#!/bin/sh
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:

(
	cd words
	for i in *
	do 
		echo uniqing $i
		sort $i | uniq > ../uniq/xx
		mv ../uniq/xx ../uniq/$i
	done
)
