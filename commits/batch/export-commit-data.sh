#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation) - Initial implementation
# *******************************************************************************/
cd /home/data/users/wbeaton/dash/org.eclipse.dash/dashboard/commits/batch/

if [ ! -f password ];
then
    echo "Password file not found!"
    exit 1
fi

password=`cat password`

# Generate some useful output that we can use to generate handy charts.
mysql --user=dashboard --password=$password dashboard -Be "select yearmonth, count(distinct login) from commits group by yearmonth" > committer-count.txt

scp committer-*.txt wbeaton@dev.eclipse.org:/home/data/httpd/writable/projects/
