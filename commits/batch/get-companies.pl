#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# get-companies.pl
#
# writes:
# companies.txt

use strict;
use LWP;
use FileHandle;

my $them = "https://www.eclipse.org";
my $file = "/projects/web-api/commit-companies.php";

# Get content from web-api
my $browser = LWP::UserAgent->new;
my $result = $browser->get($them . $file) or
	die "Can't open connection to $them";

# Make sure we got something
if(length($result->decoded_content()) < 1000) {
	die "Error, unexpectedly short companies web-api output: " . $result->decoded_content();
}

# Write output to file
my $companies_file = new FileHandle(">companies.txt") or die "Can't write to companies.txt";
$companies_file->print($result->decoded_content());
$companies_file->close();
