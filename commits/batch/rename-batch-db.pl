#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# rename-batch-db.pl
#

use strict;

use DBI;

require 'dbpassconfig'; our $db_password;
chomp( $db_password );

my $dsn = 'DBI:mysql:dashboard';
my $db_user_name = 'dashboard';
my $dbh = DBI->connect($dsn, $db_user_name, $db_password);

print "Dropping old table..\n";
my $sth = $dbh->prepare(qq{
DROP TABLE commits;
});
$sth->execute();
$sth->finish();

print "Renaming new table..\n";
$sth = $dbh->prepare(qq{
RENAME TABLE commits_batch TO commits;
});
$sth->execute();
$sth->finish();

print "Tables are..\n";
$sth = $dbh->prepare(qq{
SHOW TABLES;
});
$sth->execute();
while (my ($name) = 
    $sth->fetchrow_array())  # keep fetching until 
                             # there's nothing left
{
     print "$name\n";
}
$sth->finish();

$dbh->disconnect();
print "Complete.\n";
