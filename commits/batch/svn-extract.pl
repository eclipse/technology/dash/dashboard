#!/usr/bin/perl
# Copyright (c) 2007 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
#
# Subversion version of extract.pl
#
# usage:
# cat svn-roots.txt | ./svn-extract.pl > TEXT_FILE_OF_SVN_LOGS
#
# alternate:
# svn-extract.pl --ignoretime

use strict;
my $ignoretime = 0;
$_ = shift;
$ignoretime = /\-\-ignoretime/;

my $svnurl = "file://";

my %lastrevision;
my $last = 0;
if (-r 'last.revisions' && !$ignoretime) {
	$last = 1;
	%lastrevision = split /\s+/, `cat last.revisions`;
} else {
	print stderr "full extract\n";
}

my ($proj, $path);

for (<STDIN>) {
	chomp;
	($proj, $path) = split /\t/;

	$path = $svnurl . $path;

	if ($last) {
		$last = $lastrevision{$proj} + 1;
		chomp($last); chomp($last);
		if(length($last) < 1) {
			$last = 1;
		}
		print stderr "incremental extract from $last for $proj\n";
		print "~~~~~PROJECT: $proj\n";
		system ("svn log --verbose --non-interactive --revision $last:HEAD $path");
	} else {
		print "~~~~~PROJECT: $proj\n";
		system("svn log --verbose --non-interactive $path");
	}
}
