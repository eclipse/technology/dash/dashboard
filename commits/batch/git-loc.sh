#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2012 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation) - initial API and implementation
# *******************************************************************************/

projects=`wget http://www.eclipse.org/projects/releases/projects.php -O - 2> /dev/null | paste -s -d '|'`
wget http://www.eclipse.org/projects/web-api/repos.php?type=git -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $2}' | sort | uniq | head -n 2 | xargs -I {} bash -c 'git clone git://git.eclipse.org{}'



wget http://www.eclipse.org/projects/web-api/repos.php?type=cvs -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $1}' | sort uniq | sed "s|\(/cvsroot/[^/]*\)/|cvs -d :pserver:anonymous@dev.eclipse.org:\1 checkout |" 

wget http://www.eclipse.org/projects/web-api/repos.php?type=git -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $1}' | uniq | wc -l > projects-git.txt
wget http://www.eclipse.org/projects/web-api/repos.php?type=git -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $2}' | uniq | xargs -I {} bash -c 'cd {}; git archive --format=tar HEAD | sed '/^\s*$/d' | wc -l' | paste -sd+ | bc > loc-git.txt
wget http://www.eclipse.org/projects/web-api/repos.php?type=git -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $2}' | uniq | xargs -I {} bash -c 'cd {}; git archive --format=tar HEAD | wc -c' | paste -sd+ | bc > bytes-git.txt
wget http://www.eclipse.org/projects/web-api/repos.php?type=git -O - 2> /dev/null | egrep "^(${projects})[^\.]"| wc -l > count-git.txt

#CVS
projects=`wget http://www.eclipse.org/projects/releases/projects.php -O - 2> /dev/null | paste -s -d '|'`
wget http://www.eclipse.org/projects/web-api/repos.php?type=cvs -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $1}' | uniq | wc -l > projects-cvs.txt
wget http://www.eclipse.org/projects/web-api/repos.php?type=cvs -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $2}' | uniq | grep -v webtools$ | xargs -I {} bash -c 'find {} -type f -print0' | grep -FzZv /Attic | xargs -0 -I {} bash -c "co -p '{}' 2> /dev/null | sed '/^\s*$/d' | wc -l" | paste -sd+ | bc > loc-cvs.txt
wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=cvs -O - 2> /dev/null | egrep "^(${projects})[^\.]"|  awk -F'\t' '{print $2}' |uniq| xargs -I {} bash -c 'find {} -type f -print0' | grep -FzZv /Attic | xargs -0 -I {} bash -c "co -p '{}' 2> /dev/null | wc -c" | paste -sd+ | bc > bytes-cvs.txt

#SVN
projects=`wget http://www.eclipse.org/projects/releases/projects.php -O - 2> /dev/null | paste -s -d '|'`
wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=svn -O - 2> /dev/null | egrep "^(${projects})[^\.]"| awk -F'\t' '{print $2}' | uniq | xargs -I {} bash -c "svn list -R svn://dev.eclipse.org{}|grep '^trunk'|awk '{print \"svn://dev.eclipse.org/svnroot{}/\" \$1}'" | xargs -I {} bash -c "svn cat '{}' 2> /dev/null | wc -l" | paste -sd+ | bc > loc-svn.txt