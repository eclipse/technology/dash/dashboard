#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation) - Initial implementation
# *******************************************************************************/
cd /home/data/users/wbeaton/dash/org.eclipse.dash/dashboard/commits/batch/

if [ ! -f password ];
then
    echo "Password file not found!"
    exit 1
fi

password=`cat password`

# Generate some useful output that we can use to generate handy charts.
mysql --user=dashboard --password=$password dashboard -Be "select project, yearmonth as period, count(distinct revision) as count from commits group by project, yearmonth order by yearmonth" > project-period-commit-count.txt
mysql --user=dashboard --password=$password dashboard -Be "select project, company, count(distinct revision) as count from commits where date > NOW() - INTERVAL 3 MONTH group by project, company"  > project-company-commit-count.txt
mysql --user=dashboard --password=$password dashboard -Be "select project, login, company, count(distinct revision) as count from commits where date > NOW() - INTERVAL 3 MONTH group by project, login"  > project-login-commit-count.txt
mysql --user=dashboard --password=$password dashboard -Be "select yearmonth, count(distinct project) from commits group by yearmonth order by yearmonth"  > project-active-count.txt

scp project-*-count.txt wbeaton@build.eclipse.org:/home/data/httpd/writable/projects/
