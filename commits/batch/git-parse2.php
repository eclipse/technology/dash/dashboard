#!/usr/bin/php
<?php
/*******************************************************************************
 * Copyright (c) 2010, 2012 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/
error_reporting(E_ALL);
ini_set("display_errors", true);

/**
 * This script parses the output of the git log command (with the --numstat
 * option to include file names) and generates the tabular output format
 * required by Dash.
 * 
 * Examples: 
 *  echo -e "technology.dash.woolsey\t/gitroot/woolsey/iplog/.git/" | ./git-author.php
 *  wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=git -O - 2> /dev/null | php git-extract.php | php git-parse2.php
 *  wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=git -O - 2> /dev/null | grep eclipse.orion | php git-extract.php |  php git-parse.php
 *  
 * @author Wayne Beaton
 */

require_once dirname(__FILE__) . '/../common/git-functions.inc';
require_once dirname(__FILE__) . '/../common/git-commits.inc';
require_once dirname(__FILE__) . '/../common/common-functions.inc';

$headers = array(
	"DATE", "YEAR", "YEARMONTH","YEARMONTHDAY",
	"TOPPROJECT","PROJECT","FILENAME","FILETYPE","REVISION",
	"CHANGE_SIZE","MESSAGE_SIZE","LOGIN","COMPANY");

echo "#" . implode("\t", $headers) . "\n";

parseGitLog(STDIN, new GitCommitProcessor());

?>