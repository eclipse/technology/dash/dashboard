<?php 
#/*******************************************************************************
# * Copyright (c) 2013 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation) - Initial implementation
# *******************************************************************************/

/*
 * Pull the git repo information from the PMI. Format it in the manner expected
 * by the ./git-export.php script
 */
$json = file_get_contents('https://projects.eclipse.org/json/repos/git');
foreach(json_decode($json) as $project => $repos) {
	foreach ($repos as $repo) {
		echo "$project\t$repo\n";
	}
}
?>
