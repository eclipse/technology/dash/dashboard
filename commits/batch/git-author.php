#!/usr/bin/php
<?php
/*******************************************************************************
 * Copyright (c) 2010, 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/
error_reporting(E_ALL);
ini_set("display_errors", true);

/**
 * This script parses the output of the git log command (with the --numstat
 * option to include file names) and generates the tabular output format
 * required by Dash.
 * 
 * Usage:
 * 
 *  git --git-dir=/gitroot/woolsey/iplog/.git/ log --numstat | php git-author.php
 *  
 * Examples: 
 *  echo -e "technology.dash.woolsey\t/gitroot/woolsey/iplog/.git/" | ./git-author.php
 *  wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=git -O - 2> /dev/null | ./git-author.php
 *  wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=git -O - 2> /dev/null | grep eclipse.orion | ./git-author.php
 *  
 * @author Wayne Beaton
 */

require_once dirname(__FILE__) . '/../common/git-functions.inc';
require_once dirname(__FILE__) . '/../common/git-logcache.inc';
require_once dirname(__FILE__) . '/../common/common-functions.inc';

while (!feof(STDIN)) {
	$line = fgets(STDIN);
	if (!trim($line)) continue;
	
	$parts = split("\t", $line);

	$projectid = trim($parts[0]);
	$path = trim($parts[1]);
	
	if (!$path) continue;
	
	updateGitLogCache(realpath($path), true);
}

?>