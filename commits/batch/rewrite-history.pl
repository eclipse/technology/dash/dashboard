#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# update-history.pl
#

use strict;

use DBI;

sub writelog {
  my $line = shift;
  my $logfilename = "logs/inserts.log";
  my $date = `date`;
  chomp($date);
  open LOG, ">>$logfilename";
  print LOG $date . " " . $line . "\n";
  close LOG;
}

require 'dbpassconfig'; our $db_password;
chomp( $db_password );

my $dsn = 'DBI:mysql:dashboard:dashdbhost';
my $db_user_name = 'dashboard';
my $dbh = DBI->connect($dsn, $db_user_name, $db_password);

my $line;
my $sth;

while( $line = <STDIN> ) {
  chomp($line);
  $sth = $dbh->prepare($line) or writelog( "prepare error: " . $dbh->errstr );
  $sth->execute( ) or writelog( "query error: " . $sth->errstr );
}

$sth->finish();
$dbh->disconnect();
