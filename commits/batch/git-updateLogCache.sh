#!/bin/bash
#/*******************************************************************************
# * Copyright (c) 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Ward Cunningham, Bjorn Freeman-Benson (Eclipse Foundation)
# *		- initial API and implementation
# *    Karl Matthias (Eclipse Foundation)
# *    Wayne Beaton (Eclipse Foundation) - Bug 334531, Bug 351584
# *******************************************************************************/
wget http://www.eclipse.org/projects/web-api/roots-generator.php?type=git -O - 2> /dev/null | php /home/data/users/wbeaton/dash/org.eclipse.dash/dashboard/commits/batch/git-author.php