#!/bin/sh
# Copyright (c) 2006-2009 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson, Karl Matthias
#
echo weekly run -- `date`
sh run-batch.sh
echo run complete -- `date`
