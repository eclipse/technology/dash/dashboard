#!/usr/bin/perl
#/*******************************************************************************
# * Copyright (c) 2006 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Ward Cunningham, Bjorn Freeman-Benson (Eclipse Foundation)
# *		- initial API and implementation
# *    Wayne Beaton (Eclipse Foundation) - Bug 334531
# *******************************************************************************/
#
# usage:
# cat roots.txt | ./extract.pl > TEXT_FILE_OF_RLOGS
#
# alternate:
# extract.pl --ignoretime

use strict;
my $dir = `pwd`;
my $ignoretime = 0;
$_ = shift;
$ignoretime = /\-\-ignoretime/;

my $last;
if (-e 'last.time' && !$ignoretime) {
	$last = `date -r last.time +'%Y/%m/%d %H:%M:%S'`;
	print stderr "incremental extract from $last\n";
} else {
	print stderr "full extract\n";
}

my ($proj, $dir);

for (<STDIN>) {
	chomp;
	($proj, $dir) = split /\t/;
	find ('', $dir);
}

sub find {
	my ($prefix, $filename) = @_;
	#print stderr "$prefix -- $filename\n";
	chdir( $prefix) or die $!;
	return unless -r $filename;
	return unless -x $filename;
	return unless -d $filename;
	chdir ($filename) or die $!;
        $prefix .= "/$filename";
	$prefix =~ s/^\/\//\//;
	opendir (D, '.') or die $!;
	my @names = sort grep -r $_ && !/^\./, readdir(D);
	map process($prefix, $_), grep !-d $_ && /,v$/, @names;
	map find($prefix, $_), grep { (-d $_ || -l $_) } $_, @names;
	chdir( $prefix) or die $!;
}

sub process {
	my ($prefix, $filename) = @_;
	return if $filename =~ /\'/ ; # our database code cannot handle single quotes
	return if $filename =~ /^-/ ; # rlog can't handle filenames that begin with dash
	return unless -r $filename;
        print "$proj\t$prefix/$filename\n";
	if ($last) {
		system ("rlog", "-N", "-d>$last", $filename);
	} else {
		system ("rlog", "-N", $filename);
	}
}

