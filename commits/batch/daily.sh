#!/bin/sh
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
echo nightly run -- `date`
echo update
sh update.sh
echo run
sh run-incremental.sh
echo run complete -- `date`
