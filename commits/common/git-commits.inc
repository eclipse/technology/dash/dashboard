<?php
/*******************************************************************************
 * Copyright (c) 2011, 2012 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/
require_once dirname(__FILE__) . '/git-functions.inc';

class GitCommitProcessor extends GitLogProcessor {
	var $record;
	
	function __construct() {
	}

	function startCommit($ref) {
		$this->record = array('size' => 0);
	}

	function data($ref, $key, $value) {
		$this->record[$key] = $value;
	}

	function file($ref, $filename, $filetype, $size) {
		$this->record['files'][] = array(
			'name' => $filename,
			'type' => $filetype,
			'size' => $size
		);
	}

	function endCommit($ref) {
		if (!isset($this->record['files'])) return;
		
		$committer = $this->record['committer'];
		$author = $this->record['author'];
		
		$login = getCommitterId($author);
		if (!$login) $login = getCommitterId($committer);
		if (!$login) $login = $this->record['committer'];

		$year = date('Y', $this->record['date']);
		$yearmonth = date('Ym', $this->record['date']);
		$yearmonthday = date('Ymd', $this->record['date']);
		$date = date("Y/m/d", $this->record['date']);
		
		$top = $this->record['top'];
		$project = $this->record['project'];
				
		$company = getCommitterCompany($login);
		if (!$company) $company = 'unknown';
		
		$message_size = isset($this->record['comment']) ? strlen($this->record['comment']) : 0;
		
		foreach($this->record['files'] as $file) {
			$filename = $file['name'];
			$filetype = $file['type'];
			$change_size = $file['size'];

			echo "$date\t$year\t$yearmonth\t$yearmonthday\t$top\t$project\t$filename\t$filetype\t$ref\t$change_size\t$message_size\t$login\t$company\n";
		}
		
	}
}

?>