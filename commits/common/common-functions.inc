<?php
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

/**
 * This function basically throws a fit if the caller is
 * coming from anywhere outside of the EF. It determines
 * whether or not to grant access based on the IP Address.
 */
function mustBeEclipseFoundationCaller() {
	if(preg_match('/209\.217\.126\.125|206\.191\.52\.\d+|172\.25\.25\.\d+|72\.30\.206\.\d+|127\.0\.0\.1/', $_SERVER['REMOTE_ADDR'])) return;

	//	$log = fopen( $logfile, "a" );
	//	fwrite( $log, date('Y-m-d.H:i:s') . " " . $_SERVER['REMOTE_ADDR'] . " is an invalid caller\n" );
	//	fclose( $log );

	logMessage('# ' . $_SERVER['REMOTE_ADDR'] . " is an invalid caller");
	exit;
}

function openDatabaseConnection() {
	$user="dashboard";
	$database="dashboard";
	$password = getDashboardDbPassword();
	
	$handle = mysql_connect('localhost',$user,$password);
	if (!$handle) {
		logMessage("Open connection", mysql_error());
		return FALSE;
	}
	
	if (!mysql_select_db( $database, $handle )) {
		logMessage("Open connection", mysql_error());
		return FALSE;
	}
	
	return $handle;
}

function dashboard_sql($query) {
	global $dashboardHandle;
	
	if ($dashboardHandle === FALSE) return null;
	if (!$dashboardHandle) $dashboardHandle = openDatabaseConnection();
	
	logMessage("Execute", preg_replace('/\s+/', ' ', $query));
	
	$result = mysql_query($query, $dashboardHandle);
	
	if (!$result) logMessage("Query", mysql_error());
	
	return $result;
}

function logMessage($title, $message) {
	echo "# $title: $message\n";
}

/**
 * Attempt to acquire the DB password. It is either stored in a file 
 * named dbpassword.txt in this file's directory (a local override, generally 
 * used for testing) or in /var/lib/dashboard/DBPASSWORD.txt.
 * 
 * @internal
 */
function getDashboardDbPassword() {
	if ($password = getDashboardDbPasswordFromFile(getenv("HOME") . '/dbpassword.txt')) return $password;
	if ($password = getDashboardDbPasswordFromFile('/var/lib/dashboard/DBPASSWORD.txt')) return $password;
	return null;
}

/**
 * @internal
 */
function getDashboardDbPasswordFromFile($password_file) {
	if (!is_file($password_file)) return null;
	if (!is_readable($password_file)) return null;
	
	return trim(file_get_contents($password_file));	
}

?>