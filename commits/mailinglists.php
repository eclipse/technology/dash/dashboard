<?php
/*******************************************************************************
 * Copyright (c) 2010, 2012 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Wayne Beaton (Eclipse Foundation)- initial API and implementation
*******************************************************************************/
require_once dirname(__FILE__) . '/common/common-functions.inc';

captureMailingLists('eclipse.org', 'https://dev.eclipse.org/mailman/listinfo');
captureMailingLists('locationtech.org', 'https://www.locationtech.org/mailman/listinfo');
captureMailingLists('polarsys.org', 'https://polarsys.org/mailman/listinfo');

function captureMailingLists($domain, $url) {
	createMailingListsTable();
	$lists = array();
	if ($file = fopen($url, 'r')) {
		while ($line = fgets($file)) {
			if (preg_match('/"listinfo\/([^"]+)"/', $line, $matches)) {
				$name = $matches[1];
				$page = "${url}/${name}";
				$email = "$name@$domain";
				dashboard_sql("insert ignore into mailinglists (domain, name, url, email) values ('$domain', '$name', '$page', '$email');");
			}	
		}
		fclose($file);
	}
	return $lists;
}

function createMailingListsTable() {
	$sql = "create table if not exists mailinglists (
			domain varchar(20) not null,
			name varchar(100) not null,
			url varchar(100),
			email varchar(200),
			ts timestamp default current_timestamp on update current_timestamp,
			primary key (domain, name)
		);";
	dashboard_sql(preg_replace('/\s/', ' ', $sql));
}
?>