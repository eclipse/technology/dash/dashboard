<html>
<head>
<title>2008 Roadmap Freezer</title>
</head>
<body>
<?php

$destdir = '/home/bfreeman/public_html/';

ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Eclipse Roadmap v4</title><meta name="author"/>
<meta name="keywords" content="roadmap, projects, strategy, documents, about, foundation" /><link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/small.css" title="small" /><link rel="alternate stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/large.css" title="large" /><link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/visual.css" media="screen" /><link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/layout.css" media="screen" />
<!--[if IE]> 	<link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/ie_style.css" media="screen"/> <![endif]-->
<!--[if IE 6]> 	<link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/ie6_style.css" media="screen"/> <![endif]-->
<link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/header.css" media="screen" />
<script type="text/javascript" src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/styleswitcher.js"></script>
</head>
<body>
<div id="header">
	<div id="header-graphic" class="eclipse-main">
		<a href="http://www.eclipse.org/"><img src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/eclipse_home_header.jpg" alt="" /></a><h1>Eclipse</h1>	
	</div>

	<div id="header-global-holder" class="eclipse-main-global">
		<div id="header-global-links"><ul>
<li><a href="http://www.eclipse.org/org/foundation/contact.php" class="first_one">Contact</a></li><li><a href="http://www.eclipse.org/legal/">Legal</a></li>
			</ul>
		</div>
		<div id="header-icons">
<a href="http://live.eclipse.org"><img src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/Icon_Live.png" width="28" height="28" alt="Eclipse Live" title="Eclipse Live" /></a>
<a href="http://www.eclipseplugincentral.com"><img src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/Icon_plugin.png" width="28" height="28" alt="Eclipse Plugin Central" title="Eclipse Plugin Central" /></a>
<a href="http://www.planeteclipse.org"><img src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/Icon_planet.png" width="28" height="28" alt="Planet Eclipse" title="Planet Eclipse" /></a>
		</div>

	</div></div><div id="header-menu"><div id="header-nav">
		<ul>
		<li><a class="first_one" href="http://www.eclipse.org/" target="_self">Home</a></li> 
				<li><a  href="http://www.eclipse.org/users/" target="_self">Users</a></li> 
				<li><a  href="http://www.eclipse.org/membership/" target="_self">Members</a></li> 
				<li><a  href="http://www.eclipse.org/committers/" target="_self">Committers</a></li> 
				<li><a  href="http://www.eclipse.org/downloads/" target="_self">Downloads</a></li> 
				<li><a  href="http://www.eclipse.org/resources/" target="_self">Resources</a></li> 
				<li><a  href="http://www.eclipse.org/projects/" target="_self">Projects</a></li> 
				<li><a  href="http://www.eclipse.org/org/" target="_self">About Us</a></li> 
				</ul>

	</div>
	<div id="header-utils">
		<form action="http://www.google.com/cse" id="searchbox_017941334893793413703:sqfrdtd112s">
	 	<input type="hidden" name="cx" value="017941334893793413703:sqfrdtd112s" />
  		<input type="text" name="q" size="25" />
  		<input type="submit" name="sa" value="Search" />
		</form>
		<script type="text/javascript" src="http://www.google.com/coop/cse/brand?form=searchbox_017941334893793413703%3Asqfrdtd112s&lang=en"></script>		<ul>

						<li class="text_size"><a class="smallText" title="Small Text" href="#" onclick="setActiveStyleSheet('small');return false;">A</a> <a class="largeText" title="Large Text" href="#" onclick="setActiveStyleSheet('large');return false;">A</a></li>
		</ul>
	</div></div><div id="leftcol">
<ul id="leftnav">
<li class="separator"><a class="separator" href="index.php">About Us &#160;&#160;<img src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/leftnav_bullet_down.gif" border="0" alt="" /></a></li>
<li><a href="http://www.eclipse.org/org/foundation/">Foundation</a></li>
<li><a href="http://www.eclipse.org/org/documents/">Governance</a></li>
<li><a href="http://www.eclipse.org/legal/">Legal Resources</a></li>

<li><a href="http://www.eclipse.org/org/foundation/contact.php">Contact Us</a></li>
<li style="background-image: url(/eclipse.org-common/themes/Phoenix/images/leftnav_fade.jpg); background-repeat: repeat-x; border-style: none;"><br /><br /><br /><br /><br /><br /><br /><br />
</li>
</ul>

</div>
<div id="container">
    <div id="maincontent">

	<div id="midcolumn">
<?php
$headertext = ob_get_contents();
ob_end_clean();

ob_start();
?>
	</div>
	
	<div id="rightcolumn">
	    <div class="sideitem">
			<h6>Thank you!</h6>
			<p>Our thanks to <a href="../../foundation/thankyou.php">HP, IBM, Intel, Magma and Novell</a> for generous donations to our website infrastructure.</p>
		</div>

		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="../../../projects/">Projects</a></li>
				<li><a href="../../../membership/">Membership</a></li>
				<li><a href="../../../membership/become_a_member/">Become a member</a></li>
			</ul>

		</div>
	</div>
	</div>

		<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
		var pageTracker = _gat._getTracker("UA-910670-2");
		pageTracker._initData();
		pageTracker._trackPageview();
		</script></div><div id="footer">
<ul id="footernav">
<li class="first"><a href="http://www.eclipse.org/">Home</a></li>
<li><a href="http://www.eclipse.org/legal/privacy.php">Privacy Policy</a></li>

<li><a href="http://www.eclipse.org/legal/termsofuse.php">Terms of Use</a><a href="http://www.eclipse.org/org/press-release/20081112_termsofuse.php"><img src="http://www.eclipse.org/images/updated.png" align="absmiddle" /></a></li>
</ul>
<p>Copyright &copy; 2008 The Eclipse Foundation. All Rights Reserved</p>
</div></body></html>
<?php
$footertext = ob_get_contents();
ob_end_clean();

//-----------------------------------------------------

echo "index.html..."; flush();
$contents = file_get_contents( 'http://wiki.eclipse.org/index.php?title=2008_Roadmap&printable=yes' );

$pos1 = strpos( $contents, "<!-- start content -->" );
$pos2 = strpos( $contents, "<!-- end content -->" );
$contents = substr($contents, $pos1, $pos2 );

$pos = strpos( $contents, '<div class="printfooter">' );
$contents = substr($contents, 0, $pos);

$contents = str_replace( 
		array( '<a href="/RequirementsCouncilThemesAndPriorities"', 'Platform Release Plan' ),
		array( '<a href="themesandpriorities.html"', '<a href="platformplan.html">Platform Release Plan</a>' ), 
		$contents );

$contents = $headertext . '<h1>2008 Roadmap</h1>' . $contents . $footertext;

file_put_contents( $destdir . 'index.html', $contents );
echo "<br>"; flush();

//-----------------------------------------------------

echo "themesandpriorities.html..."; flush();
$contents = file_get_contents( 'http://wiki.eclipse.org/index.php?title=RequirementsCouncilThemesAndPriorities&printable=yes' );

$pos1 = strpos( $contents, "<!-- start content -->" );
$pos2 = strpos( $contents, "<!-- end content -->" );
$contents = substr($contents, $pos1, $pos2 );

$pos = strpos( $contents, '<div class="printfooter">' );
$contents = substr($contents, 0, $pos);

$contents = $headertext . '<h1>Themes and Priorities</h1>' . $contents . $footertext;

file_put_contents( $destdir . 'themesandpriorities.html', $contents );
echo "<br>"; flush();

//-----------------------------------------------------

$contents = file( 'http://www.eclipse.org/projects/web-api/roots-generator.php' );
$projects = array();
foreach( $contents as $line ) {
	$words = explode( "\t", $line );
	$projects[$words[0]] = 1;
}
$projects = array_keys($projects);
sort( $projects );

$count = 0;
foreach( $projects as $project ) {
	$count++;
	echo $project . '_plan.html...'; flush();
	$contents = file_get_contents( 'http://www.eclipse.org/projects/project-plan.php?projectid=' . $project );

	$contents = str_replace( 
		array( 'src="/', 
			   'href="/', 
			   '<div style="float: right; text-align: right"><a href="?projectid='  ),
		array( 'src="http://www.eclipse.org/',
			   'href="http://www.eclipse.org/',
			   '<div style="visibility: hidden; float: right; text-align: right"><a href="?projectid=' ), 
		$contents );

	file_put_contents( $destdir . $project . '_plan.html', $contents );
	echo "<br>"; flush();
}

echo "platformplan.html..."; flush();
$contents = '<ul>';
foreach( $projects as $project ) {
	$contents .= "<li><a href='${project}_plan.html'>$project</a></li>\n";
}
$contents .= '</ul>';

$contents = $headertext . '<h1>Project Plans</h1>' . $contents . $footertext;

file_put_contents( $destdir . 'platformplan.html', $contents );
echo "<br>"; flush();

?>
2008 roadmap frozen and written to <a href="http://dash.eclipse.org/~bfreeman/">dash.eclipse.org/~bfreeman</a><br>
<?= date('r') ?>
</body>
