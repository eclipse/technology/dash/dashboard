<?php
/*******************************************************************************
 * Copyright (c) 2010, 2012 Eclipse Foundation and others.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* http://www.eclipse.org/legal/epl-v10.html
*
* Contributors:
*    Wayne Beaton (Eclipse Foundation)- initial API and implementation
*******************************************************************************/
require_once dirname(__FILE__) . '/common/common-functions.inc';

$path = '/gitroot/';
//$path = '/home/wayne/git/www';

scanForRepositories($path);

function scanForRepositories($path) {
	createTables();
	scanForGitRepositories($path);
	scanForSvnRepositories('/svnroot/');
}

function scanForGitRepositories($path) {
	$find="find $path -type d -name *.git | xargs -I '{}' git --git-dir={} rev-parse --git-dir 2> /dev/null";
	$handle = popen($find, 'r');
	while ($repository = fgets($handle)) {
		// Skip web directories
		if (preg_match('/^\/gitroot\/www\.eclipse\.org\//', $repository)) continue;
		
		// Skip the gerrit directory
		if (preg_match('/^\/gitroot\/gerrit\//', $repository)) continue;
		captureGitRepository(trim($repository));
	}
	pclose($handle);
}

function scanForSvnRepositories($path) {
// 	$find="find /svnroot/ -type f -name fs-type 2> /dev/null | sed 's|/db/fs-type||'";
// 	$handle = popen($find, 'r');
// 	while ($repository = fgets($handle)) {
// 		captureSvnRepository(trim($repository));
// 	}
// 	pclose($handle);

	/*
	 * /svnroot/ isn't accessible on Pebbles. I could ask webmaster, but what's 
	 * the point? We're not making any new SVN repositories. Instead here's a
	 * hard-coded list of the SVN repositories we have in Oct 2012.
	 */
	$svn = "/svnroot/modeling/org.eclipse.gmt.am3
/svnroot/modeling/org.eclipse.emft.henshin
/svnroot/modeling/test.tmf.xtext
/svnroot/modeling/org.eclipse.mdt.sphinx
/svnroot/modeling/test.m2t.xpand
/svnroot/modeling/org.eclipse.emft.edapt
/svnroot/modeling/org.eclipse.epsilon
/svnroot/modeling/org.eclipse.emft.facet
/svnroot/modeling/org.eclipse.emft.facet.back
/svnroot/modeling/org.eclipse.gmt.viatra2
/svnroot/modeling/org.eclipse.mdt.papyrus
/svnroot/modeling/org.eclipse.emft.featuremodel
/svnroot/modeling/org.eclipse.emf.egf
/svnroot/modeling/org.eclipse.gmt.amw
/svnroot/modeling/org.eclipse.mdt.modisco
/svnroot/technology/org.eclipse.imp
/svnroot/technology/eu.geclipse
/svnroot/technology/org.eclipse.scout
/svnroot/technology/org.eclipse.stem
/svnroot/technology/org.eclipse.higgins
/svnroot/technology/org.eclipse.vtp
/svnroot/technology/org.eclipse.rtsc
/svnroot/technology/org.eclipse.osee
/svnroot/technology/org.eclipse.subversive
/svnroot/sandbox
/svnroot/rt/org.eclipse.jetty
/svnroot/rt/org.eclipse.gemini.jpa
/svnroot/rt/org.eclipse.gemini
/svnroot/rt/org.eclipse.persistence
/svnroot/rt/org.eclipse.gemini.management
/svnroot/rt/org.eclipse.smila
/svnroot/tools/org.eclipse.buckminster
/svnroot/tools/org.eclipse.mylyn.reviews
/svnroot/tools/org.eclipse.tcf
/svnroot/tools/org.eclipse.buckminster.back
/svnroot/tools/org.eclipse.objectteams
/svnroot/tools/org.eclipse.windowbuilder
/svnroot/tools/org.eclipse.mat
/svnroot/eclipse/org.eclipse.ufacekit.back
/svnroot/eclipse/org.eclipse.ufacekit
/svnroot/soa/org.eclipse.ebpm.120612
/svnroot/soa/org.eclipse.stp.sca-tools
/svnroot/soa/org.eclipse.swordfish
/svnroot/soa/org.eclipse.ebam
/svnroot/soa/org.eclipse.jwt
/svnroot/soa/org.eclipse.ebpm
/svnroot/soa/org.eclipse.mangrove
/svnroot/soa/org.eclipse.ebpm.bad";
	foreach(split("\n", $svn) as $path) {
		captureSvnRepository($path);
	}
}

function createTables() {
	$sql = "create table if not exists repositories (
			name varchar(100) not null,
			path varchar(255) not null primary key,
			git varchar(255),
			ssh varchar(255),
			gerrit varchar(255),
			http varchar(255),
			github varchar(255),
			google varchar(255),
			description varchar(1024),
			ts timestamp default now() on update now()
		);";
	dashboard_sql(preg_replace('/\s/', ' ', $sql));
}

function captureGitRepository($repository) {
	$name = mysql_real_escape_string(getName($repository));
	$git = mysql_real_escape_string("git://git.eclipse.org$repository");
	$ssh = mysql_real_escape_string("ssh://userid@git.eclipse.org$repository");
	$gerrit = mysql_real_escape_string(getGerritUrl($repository));
	$http = mysql_real_escape_string(getHttpUrl($repository));
	$google = mysql_real_escape_string(getGoogleSourceUrl($repository));
	$description = mysql_real_escape_string(getDescription($repository));
	
	dashboard_sql("insert ignore into repositories (name, path) values('$name', '$repository')");
	dashboard_sql("update repositories set name='$name', git='$git', ssh='$ssh', gerrit='$gerrit', google='$google', http='$http', description='$description' where path='$repository'");
}

function captureSvnRepository($path) {
	if (!preg_match('/^\/svnroot\/([^\/]+)\/((?:org\.eclipse\.)?(.+))$/', $path, $matches)) return;
	$name = mysql_real_escape_string($matches[3]);
	$repo = mysql_real_escape_string($matches[2]);
	$toplevel = mysql_real_escape_string($matches[1]);
	
	$http = "http://dev.eclipse.org/svnroot/$toplevel/$repo";
	if (!isValidUrl($http)) return;
	
	dashboard_sql("insert ignore into repositories (name, path) values('$name', '$path')");
	dashboard_sql("update repositories set name='$name', ssh='', http='$http', description='The project\'s SVN Repository' where path='$path'");
}

function getName($path) {
	if (preg_match('/org\.eclipse\.(.*)\.git$/', $path, $matches)) {
		return $matches[1];
	} else if (preg_match('/org\.eclipse\.(.*)$/', $path, $matches)) {
		return $matches[1];
	} else if (preg_match('/([^\/]*)\.git$/', $path, $matches)) {
		return $matches[1];
	} else if (preg_match('/([^\/]*)$/', $path, $matches)) {
		return $matches[1];
	} else {
		return $path;
	}
}

function getHttpUrl($path) {
	preg_match('/\/gitroot\/(.+)/', $path, $matches);
	$partial = $matches[1];
	return "http://git.eclipse.org/c/$partial";
} 

function getGerritUrl($path) {
	preg_match('/\/gitroot\/(.+)\.git$/', $path, $matches);
	$partial = $matches[1];
	$gerrit = "https://git.eclipse.org/r/$partial";

	$known = getGerritRepositoryData();
	if (isset($known[$partial])) return $gerrit;
	
	return '';	
}

/**
 * Pull in the list of known repositories from Gerrit.
 * 
 * TODO This only works for eclipse.org
 * 
 * @return mixed
 */
function getGerritRepositoryData() {
    $file = fopen('https://git.eclipse.org/r/projects/', 'r');
    fgets($file); // Skip the first line
    $data = '';
    while ($line = fgets($file)) $data .= $line;
    fclose($file);
    
    return json_decode($data, true);
}

/**
 * @deprecated
 * @param unknown $path
 * @return string
 */
function getGitHubUrl($path) {
	$url = "https://github.com/eclipse/" . getName($path);
	return isValidUrl($url) ? $url : '';
}

function getGoogleSourceUrl($path) {
	preg_match('/^\/gitroot\/(.*)\.git$/', $path, $matches);
	$name = $matches[1];
	$url = "https://eclipse.googlesource.com/$name/";
	return isValidUrl($url) ? $url : '';
}

function getDescription($path) {
	return @trim(file_get_contents("$path/description"));
}

function isValidUrl($url) {
	foreach(get_headers($url) as $header) {
		if (preg_match('/^HTTP.*(\d)\d\d/', $header, $matches)) {
			// We are happy with 200 or 300 codes.
			return $matches[1] == '2' || $matches[1] == '3';
		}
	}
	return false;
}
?>