#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# enhance.pl  <  TAB_FILE
#

use strict;

use Date::Manip;

my $line;
my @header;
my $dateidx;
my %data;

while( $line = <STDIN> ) {
  chomp($line);
  if( $line =~ /^#\t(.*)/ ) {
    my $header = $1;
    @header = split /\t/, $header;
    print "#YEAR	YEARMONTH	YEARMONTHDAY	TOPPROJECT	" . join( "\t", @header ) . "\n"
  } else {
    my @body = split( /\t/, $line );
    for( my $i = 0; $i < scalar( @header ); $i++ ) {
	$data{$header[$i]} = $body[$i];
	$dateidx = $i if $header[$i] eq "DATE";
    }
    #
    # extra date fields
    #
    my $d2 = UnixDate( $data{DATE}, "%Y/%m/%d" );
    $data{DATE} = $d2;
    $body[$dateidx] = $d2;
    $d2 =~ /(....)\/(..)\/(..)/;
    $data{YEAR} = $1;
    $data{YEARMONTH} = "$1$2";
    $data{YEARMONTHDAY} = "$1$2$3";
    #
    # top project
    #
    ($data{TOPPROJECT}) = $data{PROJECT} =~ /^([^\.]+).*/;
    #
    # the output line
    #
    print "$data{YEAR}	$data{YEARMONTH}	$data{YEARMONTHDAY}	$data{TOPPROJECT}	"
	. join( "\t", @body ) . "\n";
  }
}
