#!/bin/bash
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# run.sh

./get-projects.pl > projects.txt
touch start.time
cat projects.txt | ./extract.pl | ./parse.pl | ./enhance.pl | ./chunk.pl insert.pl
mv start.time last.time
touch finish.time
echo done
