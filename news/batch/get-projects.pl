#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage:
# get-projects.pl > projects.txt

$them = "www.eclipse.org";
$port = 80;
$file = "projects/web-api/dashboard-project-infos.php";

$sockaddr = 'S n a4 x8';
$AF_INET = 2;
$SOCK_STREAM = 1;
chop($hostname = `hostname`);

($name, $aliases, $proto) = getprotobyname('tcp');
($name, $aliases, $type, $len, $thisaddr) = gethostbyname($hostname);
($name, $aliases, $type, $len, $thataddr) = gethostbyname($them);

$this = pack($sockaddr, $AF_INET, 0,     $thisaddr);
$that = pack($sockaddr, $AF_INET, $port, $thataddr);

socket (S, $AF_INET, $SOCK_STREAM, $proto) || die "socket: $!";
bind(S, $this) || die "bind: $!";
connect(S, $that) || die "connect: $!";

select(S); 
$| = 1; 
print S "GET /$file\r\n";

select(STDOUT);
while (<S>) {
      print;
}
