#!/usr/bin/perl
# Copyright (c) 2006 Eclipse Foundation, made available under EPL v1.0
# Contributors Ward Cunningham, Bjorn Freeman-Benson
#
# usage: 
# chuck.pl insert.pl < TAB_FILE
#
use strict;
my $cmd = "perl " . shift;

my $h = <STDIN>;
while ($_ = <STDIN>) {
	open (S, "| $cmd");
	print S $h;
	print S;
	for (2..1000) {
		last unless $_ = <>;
		print S;
	}
}
