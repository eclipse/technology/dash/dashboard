<?php
/* Copyright (c) 2006, 2007 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt
 *
 * The REST web-api for retrieving records from the database.
 * 
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * email=name
 * replytoid=name
 * newsgroup=name
 * messageid=name
 * 
 * fields=field1,field2,field3,... 
 *   where fields can be any of: ID, 
 *	   DATE, YEAR, YEARMONTH, YEARMONTHDAY,
 *	   TOPPROJECT, PROJECT,
 *	   MESSAGEID, NEWSGROUP, 
 *     EMAIL, REPLYTOID, 
 *     REPLYDAYS 
 * 
 * If fields is not set the default fields are DISTINCT PROJECT, NEWSGROUP, MIN(DATE), MAX(DATE), COUNT(MESSAGEID)
 * 
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/news.php?email=codeslave%25&project=modeling%25
 * http://dash.eclipse.org/dash/commits/web-api/news-activity.php?email=merks%25&day=20060529
 * 
 */
header("Content-type: text/plain");
require_once "news-common.inc.php";
$allFields = "PROJECT, NEWSGROUP, MIN(DATE), MAX(DATE), COUNT(MESSAGEID)";
$fields = getFields("PROJECT, NEWSGROUP");
$where = getWHERE();

displayQuery("SELECT " . (!isset($_SERVER["QUERY_STRING"]) || !$_SERVER["QUERY_STRING"] ? $allFields : $fields) . " FROM news" . ($where ? " WHERE " . $where : "") . " GROUP BY $fields ORDER BY $fields");
?>