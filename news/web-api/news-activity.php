<?php
/* Copyright (c) 2006, 2007 Eclipse Foundation, made available under EPL v1.0
 * Contributors Ward Cunningham, Bjorn Freeman-Benson, Nick Boldt
 *
 * The REST web-api for retrieving records from the database.
 * 
 * top=name
 * project=name
 * year=yyyy
 * month=yyyymm
 * day=yyyymmdd
 * email=name
 * replytoid=name
 * newsgroup=name
 * messageid=name
 * 
 * queries={count,detail}
 * fields=field1,field2,field3,... 
 *   where fields can be any of: ID, 
 *	   DATE, YEAR, YEARMONTH, YEARMONTHDAY,
 *	   TOPPROJECT, PROJECT,
 *	   MESSAGEID, NEWSGROUP, 
 *     EMAIL, REPLYTOID, 
 *     REPLYDAYS 
 *
 * If fields is not set the default fields are DATE, MESSAGEID, NEWSGROUP, EMAIL, REPLYTOID
 * 
 * Examples:
 * http://dash.eclipse.org/dash/commits/web-api/news.php?email=codeslave%25&project=modeling%25
 * http://dash.eclipse.org/dash/commits/web-api/news-activity.php?email=merks%25&day=20060529
 * 
 * TODO: figure out why data stops at 2006-05-29
 * 
 * http://www.eclipse.org/newsgroups/index_project.php
 * http://dev.eclipse.org/newslists/news.eclipse.tools.emf/ (about 2 weeks behind)
 * http://www.eclipse.org/newsportal/thread.php?group=eclipse.tools.emf (current)
 * 
 * See also: http://dash.eclipse.org/dash/news/web-api/news-active-projects.php?fields=project,newsgroup
 *  
 */
header("Content-type: text/plain");
require_once "news-common.inc.php";
$fields = getFields("DATE, MESSAGEID, NEWSGROUP, EMAIL, REPLYTOID");
$where = getWHERE();

if (!isset($_SERVER["QUERY_STRING"]) || !$_SERVER["QUERY_STRING"])
{
	print "# Must specify at least one of the following values: top=, project=, company=, login=, year=, ...\n";
 	print "# Example: http://dash.eclipse.org/dash/commits/web-api/news.php?email=codeslave%25&project=modeling%25\n";
	exit;
}

$validQueries = array(
	"count" => "SELECT COUNT(*) FROM news" . ($where ? " WHERE " . $where : ""),
	"detail" => "SELECT $fields FROM news" . ($where ? " WHERE " . $where : "") . " GROUP BY $fields ORDER BY $fields DESC LIMIT 1000"
);

$queries = getQueries(array("count","detail")); 
if (in_array("all", $queries))
{
	foreach ($validQueries as $queryname => $query)
	{
		displayQuery($query);
	}
}
else
{
	foreach ($queries as $queryname)
	{
		if (isset($validQueries[$queryname]))
		{
			displayQuery($validQueries[$queryname]);
		}
	}
}
?>