#/*******************************************************************************
# * Copyright (c) 2011 Eclipse Foundation and others.
# * All rights reserved. This program and the accompanying materials
# * are made available under the terms of the Eclipse Public License v1.0
# * which accompanies this distribution, and is available at
# * http://www.eclipse.org/legal/epl-v10.html
# *
# * Contributors:
# *    Wayne Beaton (Eclipse Foundation) - Initial API and implementation
# *******************************************************************************/
#
# Generate a listing of all JAR files in the specified directory and all of
# its descendents. This script digs into all ZIP and TAR files as well. Files
# are output as full paths. For files found in archives, the full path of the
# archive is dumped with the file name appended.

# Usage:
# php findjars.sh /home/data/httpd/download.eclipse.org/
# (finds all the jars in all downloads served from http://download.eclipse.org)
#
# Example:
# beaton@dev2:~> ./findjars.sh /home/data/httpd/download.eclipse.org/woolsey/
# /home/data/httpd/download.eclipse.org/woolsey/nightly/site/content.jar
# /home/data/httpd/download.eclipse.org/woolsey/nightly/site/features/org.eclipse.woolsey.iplog.feature_0.5.0.201009301403.jar
# /home/data/httpd/download.eclipse.org/woolsey/nightly/site/features/org.eclipse.woolsey.iplog.feature_0.5.0.201012281305.jar
# ...
#

dir=${1:-.}
dump_zip() {
 echo $z
 for j in `zipinfo -1 $1 2>/dev/null | grep -iE '[^\/]*\.jar((\.pack)?\.gz)?$'`
 do
  echo $z/$j
 done
}

dump_tar() {
 echo $z
 for j in `gunzip -c $1 | tar --list | grep -iE '[^\/]*\.jar((\.pack)?\.gz)?$'`
 do
  echo $z/$j
 done
}

find $dir | grep -iE  '\/[^\/]*\.jar((\.pack)?\.gz)?$'
for z in `find $dir | grep -iE  '\/[^\/]*\.(zip|war|jar)?$'`; do dump_zip $z; done
for z in `find $dir | grep -iE  '\/[^\/]*\.tar.gz?$'`; do dump_tar $z; done


