#!/usr/bin/php
<?php 	
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

require_once (dirname(__FILE__) . '/classes/functions.php');
require_once (dirname(__FILE__) . '/classes/debug.php');
trace_file_info(__FILE__);

parseProjects("gatherDirectories", "doNothing");
foreach($map as $project => $directories) {
	echo "$project," . implode(',', $directories) . "\n";
}

function gatherDirectories($project, $path) {
	if (!file_exists($path)) return;
	global $map;
	
	$map[$project][] = $path;
}

function doNothing($project, $namespace) {
}

?>