<?php 	
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

/*
 * Examples:
 * cat tests/testdata.txt | php scan.php
 * ./findjars.sh /home/data/httpd/download.eclipse.org/woolsey/ | php scan.php --text
 */

/*
 * Future extension to scan bundle manifests as well.
 * Potentially useful code:
 * 
 * for f in *.jar; do echo -e "\nBundle: $f"; unzip -p $f META-INF/MANIFEST.MF | tr '\r\n' '#' | sed 's/## //g' | sed 's/##/\n/g' | grep ^Require\-Bundle\: | sed 's/[ ,]/\n\t/g'; done | less
 */
require_once (dirname(__FILE__) . '/classes/CQ.class.php');
require_once (dirname(__FILE__) . '/classes/common.php');
require_once (dirname(__FILE__) . '/classes/debug.php');
require_once (dirname(__FILE__) . '/classes/functions.php');
trace_file_info(__FILE__);

function findProjectIdFromPath($location) {
	global $_pathsToProjects;
	
	foreach($_pathsToProjects as $path => $id) {
		if (strpos($location, $path) === 0) return $id;
	}
	
	return null;
}

/**
 * Can the bundle just be ignored? Here we deal with some special cases
 * of ".jar" files we know aren't IP.
 * 
 * @param string $bundle
 * @return bool
 */
function isIgnorableBundle($bundle) {
	if ($bundle == "artifacts.jar") return true;
	if ($bundle == "artifacts.jar.pack.gz") return true;
	if ($bundle == "content.jar") return true;
	if ($bundle == "content.jar.pack.gz") return true;
	if ($bundle == "compositeContent.jar") return true;
	if ($bundle == "compositeArtifacts.jar") return true;
	if ($bundle == "pdepublishing-ant.jar") return true;
	if ($bundle == "pdepublishing.jar") return true;
	if (preg_match('/^artifacts-?\d+.jar$/', $bundle)) return true;
	if (preg_match('/^content-?\d+.jar$/', $bundle)) return true;
	if (preg_match('/^compositeArtifacts-?\d+.jar$/', $bundle)) return true;
	if (preg_match('/^compositeContent-?\d+.jar$/', $bundle)) return true;
	if (preg_match('/^com\.mycorp\.examples/', $bundle)) return true;
	if (preg_match('/^\._/', $bundle)) return true;
	return false;
}

/**
 * Given the name of a bundle, determine the Eclipse project that it
 * comes from.
 * 
 * @param string $bundle
 * @return string|null
 */
function findEclipseProjectId($bundle) {
	// Hack to handle special cases...
	// FIXME This has gotten way out of hand. Rework. Refactor.
	if (preg_match('/^org\.eclipse\.fx/', $bundle)) return 'technology.efxclipse';
	if (preg_match("/^ch\\.ethz\\.iks\\.slp/", $bundle)) return 'rt.ecf';
	if (preg_match("/^eclipselink/", $bundle)) return 'rt.eclipselink';
	if (preg_match("/^org\\.eclipse\\.persistence/", $bundle)) return 'rt.eclipselink';
	if (preg_match("/^javax\\.persistence/", $bundle)) return 'rt.eclipselink';
	
	if (preg_match("/^aspectj/", $bundle)) return 'tools.aspectj';
	
	if (preg_match("/^org\\.eclipse\\.osgi/", $bundle)) return 'rt.equinox';
	if (preg_match("/^master-equinox/", $bundle)) return 'rt.equinox';

	if (preg_match("/^jetty-6/", $bundle)) return null;
	if (preg_match("/^jetty-/", $bundle)) return 'rt.jetty';
	
	if (preg_match("/^org\\.eclipse\\.soda/", $bundle)) return 'technology.ohf';
	
	if (preg_match("/^org\\.eclipse\\.php/", $bundle)) return 'tools.pdt';
	
	if (preg_match("/^org\\.eclipse\\.draw2d/", $bundle)) return 'tools.gef';
	
	if (preg_match('/^org\.eclipse\.net4j/', $bundle)) return 'modeling.emf.cdo';
	
	if (preg_match('/^org\.eclipse\.gmf\.notation/', $bundle)) return 'modeling.gmp.gmf-notation';
	if (preg_match('/^org\.eclipse\.gmf\.runtime/', $bundle)) return 'modeling.gmp.gmf-runtime';
	if (preg_match('/^org\.eclipse\.gmf\.tooling/', $bundle)) return 'modeling.gmp.gmf-tooling';
	if (preg_match('/^org\.eclipse\.gmf/', $bundle)) return 'modeling.gmp.gmf-runtime';
	
	// Eclipse Platform Project
	$segments = array('platform', 'test', 'releng', 'webdav', 'externaltools', 'rcp', 'core', 'debug', 'help', 'ui', 'sdk', 'team', 'text', 'epp', 'ltk', 'osgi', 'search', 'compare', 'update', 'ant', 'jface', 'swt', 'jsch', 'cvs', 'tomcat', 'target', 'ftp', 'license');
	foreach($segments as $segment)
		if (preg_match("/^org\\.eclipse\\.$segment/", $bundle)) return 'eclipse.platform';
	if (preg_match('/^ecj-.*\.jar$/', $bundle)) return 'eclipse.jdt';
	
	if (preg_match('/^org\.eclipse\.wtp/', $bundle)) return 'webtools';
	if (preg_match('/^org\.eclipse\.jpt/', $bundle)) return 'webtools.dali';
	if (preg_match('/^org\.eclipse\.jst\.ws/', $bundle)) return 'webtools.webservices';
	if (preg_match('/^org\.eclipse\.wst\.ws/', $bundle)) return 'webtools.webservices';
	if (preg_match('/^org\.eclipse\.wst\.command/', $bundle)) return 'webtools.webservices';
		
	if (preg_match("/^org\\.eclipse\\.jst\\.server/", $bundle)) return 'webtools.servertools';
	if (preg_match("/^org\\.eclipse\\.jem/", $bundle)) return 'webtools.jeetools';
	if (preg_match("/^org\\.eclipse\\.jem/", $bundle)) return 'webtools.jeetools';
	if (preg_match("/^org\\.eclipse\\.wtp\\.jee\\.capabilities/", $bundle)) return 'webtools.jeetools';
	if (preg_match("/^org\\.eclipse\\.jst\\.servlet\\.ui/", $bundle)) return 'webtools.jeetools';
	
	if (preg_match('/^org\.eclipse\.jst\.common/', $bundle)) return 'webtools.common';
	if (preg_match('/^org\.eclipse\.wst\.internet\.cache/', $bundle)) return 'webtools.common';
	if (preg_match('/^org\.eclipse\.jem\.util/', $bundle)) return 'webtools.common';
	if (preg_match('/^org\.eclipse\.wst\.common/', $bundle)) return 'webtools.common';
	
	if (preg_match('/^org\.eclipse\.jst\.j2ee/', $bundle)) return 'webtools.jeetools';
	if (preg_match('/^org\.eclipse\.jst\.ejb/', $bundle)) return 'webtools.ejbtools';
	if (preg_match('/^org\.eclipse\.jem\.ejb/', $bundle)) return 'webtools.jeetools';
	
	if (preg_match('/^org\.eclipse\.jst\.jsp/', $bundle)) return 'webtools.sourceediting';
	
	if (preg_match("/^org\\.eclipse\\.wst/", $bundle)) return 'webtools.jeetools';
	if (preg_match("/^org\\.eclipse\\.jst/", $bundle)) return 'webtools.releng';
	
	if (preg_match("/^org\\.eclipse\\.wb/", $bundle)) return 'tools.windowbuilder';
		
	if (preg_match('/^objectteams-.*\.jar$/', $bundle)) return 'tools.objectteams';
	if (preg_match('/^ecotj-.*\.jar$/', $bundle)) return 'tools.objectteams';
	
	if (preg_match("/^org\\.eclipse\\.xtend/", $bundle)) return 'modeling.tmf.xtext';
	if (preg_match("/^org\\.eclipse\\.zest/", $bundle)) return 'tools.gef';
	
	if (preg_match("/^org\\.eclipse\\.rwt/", $bundle)) return 'rt.riena';
	if (preg_match('/^org\.eclipse\.rse/', $bundle)) return 'tools.tm';
	if (preg_match('/^org\.eclipse\.dstore/', $bundle)) return 'tools.tm';
	
	if (preg_match('/^org\.eclipse\.draw3d/', $bundle)) return 'technology.gef3d';
	
	if (preg_match('/^org\.eclipse\.rephraserengine/', $bundle)) return 'tools.ptp';
	if (preg_match('/^org\.gastro/', $bundle)) return 'modeling.emf.cdo';
	
	if (preg_match('/^org\.eclipse\.emf\.ocl/', $bundle)) return 'modeling.mdt.ocl';
	
	if (preg_match('/^org\.aspectj/', $bundle)) return 'tools.aspectj';
	
	if (preg_match('/^com\.ibm\.uml2\.articles_2\.0\.0\./', $bundle)) return 'modeling.mdt.uml2';
	if (preg_match('/^uml2\.articles\.jar/', $bundle)) return 'modeling.mdt.uml2';
	if (preg_match('/^org\.eclipse\.m2m\.atl/', $bundle)) return 'modeling.mmt.atl';
	if (preg_match('/^org\.eclipse\.emf\.compare/', $bundle)) return 'modeling.emfcompare';
	
	if (preg_match('/^org\.eclipse\.remote/', $bundle)) return 'tools.ptp';

	if (preg_match('/^carnot-/', $bundle)) return 'soa.stardust';
	if (preg_match('/^ipp-/', $bundle)) return 'soa.stardust';
	if (preg_match('/^stardust-/', $bundle)) return 'soa.stardust';
	if (preg_match('/-eclipse01.jar$/', $bundle)) return 'soa.stardust';
	
	if (preg_match('/^org\.eclipse\.gymnast/', $bundle)) return 'modeling.emft.emfatic';
	
	// Try to guess based on the namespaces we've assembled from the projects.
	global $_namespaceToProjects;
	
	foreach ($_namespaceToProjects as $namespace => $id) {
		if (preg_match("/^$namespace/", $bundle)) return $id;
	}

	return null;
}

/*
 * 
 */
function getBundlePattern($bundle) {
	// FIXME There is redundancy with CQ.class.php
	preg_match('/^([a-zA-Z0-9\.\-_]+[_-](\d+\.){3})(.*\.)?jar/', $bundle, $matches);
	if (count($matches) > 0) {
		$pattern = $matches[1];
		return "$pattern*.jar";
	} else {
		return $bundle;
	}
}

class ProjectInfo {
	var $id;
	var $included = array();
	var $bundles = array();
	
	function __construct($id) {
		$this->id = $id;
	}
	
	function addProject($id, $bundle) {
		if ($id == $this->id) return;
		if ($id == 'tools.orbit') return; // Orbit doesn't count
		$this->included[$id][$bundle] = true;
	}
	
	function addBundle($bundle, $location) {
		$pattern = getBundlePattern($bundle);
		if (!isset($this->bundles[$pattern])) $this->bundles[$pattern] = new BundleInfo($this, $pattern);
		$this->bundles[$pattern]->addLocation($location);
	}
	
	function findCQs($bundle, &$cqs) {
		$cq = findCQ($bundle, $this->id, true, true);
		if ($cq) {
			//echo "Found $cq->id\n";
			$cqs[] = $cq;
			return;
		}

		foreach ($this->included as $included => $ignored) {
			//echo "Checking $included\n";
			$cq = findCQ($bundle, $included, true, true);
			if ($cq) $cqs[] = $cq;
		}
	}
}

function findCQ($bundle, $projectid, $check_parents, $check_children) {
	if ($cq = find_cq_for_bundle($bundle, $projectid, $check_parents, $check_children)) return $cq;
	if (preg_match('/^com\.springsource\.(.+)$/', $bundle, $matches)) {
		$cq = find_cq_for_bundle($matches[1], $projectid, $check_parents, $check_children);
		return $cq;
	}
}

class BundleInfo {
	var $project;
	var $name;
	var $locations = array();
	var $cqs;
	
	function __construct(&$project, &$name) {
		$this->project = $project;
		$this->name = $name;
	}
	
	function addLocation(&$location) {
		if (in_array($location, $this->locations)) return;
		$this->locations[] = $location;
	}
	
	function getCQs() {
		if ($this->cqs === null) {
			$this->cqs = array();
			$this->project->findCQs($this->name, $this->cqs);
		}
		return $this->cqs;
	}
}

function dumpAsXml($projects) {
	echo "<downloads>\n";
	foreach($projects as $project) {
		echo "\t<project id=\"$project->id\">\n";
		foreach ($project->included as $included => $bundles) {
			echo "\t\t<includes id=\"$included\"/>\n";
// 			echo "\t\t<includes id=\"$included\">\n";
// 			foreach($bundles as $bundle => $ignore) 	
// 				echo "\t\t\t<bundle id=\"$bundle\">\n";
// 			echo "\t\t</includes>\n";
		}
		foreach ($project->bundles as $bundle) {
			$name = $bundle->name;
			echo "\t\t<bundle name=\"$name\">\n";
			foreach ($bundle->locations as $path) {
				echo "\t\t\t<location path=\"$path\"/>\n";
			}
			foreach ($bundle->getCQs() as $cq) {
				echo "\t\t\t<cq id=\"$cq->id\" project=\"$cq->project\"/>\n";
			}
			echo "\t\t</bundle>\n";
		}
		echo "\t</project>\n";
	}
	echo get_trace_xml();
	echo "</downloads>\n";
}

function dumpAsList($projects, $showLocations = true) {
	foreach($projects as $project) {
		echo "Project: $project->id\n";
		foreach ($project->included as $included => $ignore) {
			echo "\tIncludes: $included\n";
		}
	
		echo "\tHave CQ:\n";
		foreach ($project->bundles as $bundle) {
			if (!$bundle->getCQs()) continue;
			echo "\t\t$bundle->name ";
			foreach ($bundle->getCQs() as $cq) {
				echo "CQ$cq->id ";
			}
			echo "\n";
		}
		
		echo "\tNo CQ:\n";
		foreach ($project->bundles as $bundle) {
			if ($bundle->getCQs()) continue;
			$name = $bundle->name;
			echo "\t\t$name\n";
			if ($showLocations) {
				foreach ($bundle->locations as $path) {
					echo "\t\t\t$path\n";
				}
			}
		}
	}
	echo get_trace_text();
}

load_cqs_from_server();
$skipThingsThatDontLookLikeBundles = hasCommandLineArgument('--skip');
bootstrap();
$projects = array();
while (!feof(STDIN)) {
	$line = fgets(STDIN);
	$matches = array();
	if (!preg_match('/^(.*)\/([^\/]*\.jar(\.pack(\.gz)?)?)$/', $line, $matches)) continue;
	$location = $matches[1];
	$bundle = $matches[2];
	
	trace("Found bundle $bundle in $location");
	
	if ($skipThingsThatDontLookLikeBundles) {
		if (!preg_match('/^([a-zA-Z0-9\.\-_]+[_-](\d+\.){3})(.*\.)?jar/', $bundle)) continue;;
	}
	
	if (isIgnorableBundle($bundle)) continue;
	
	$projectId = findProjectIdFromPath($location);
	if (!$projectId) {
		trace("Can't map path $location to a project");
		continue; // TODO Handle missing paths
	}

	if (!isset($projects[$projectId])) $projects[$projectId] = new ProjectInfo($projectId);
	$info = $projects[$projectId];
	
	$eclipseProjectId = findEclipseProjectId($bundle);
	if ($eclipseProjectId) {
		trace("Bundle $bundle is from Eclipse project $eclipseProjectId");
		$info->addProject($eclipseProjectId, $bundle);
		continue;
	}
	
	if ($skipThingsThatDontLookLikeBundles) {
		if (preg_match('/^org\.eclipse\./', $bundle)) continue;
	}
	
	$info->addBundle($bundle, $location);
}

if (hasCommandLineArgument('--text')) {
	dumpAsList($projects);
} else {
	dumpAsXml($projects);
}


?>