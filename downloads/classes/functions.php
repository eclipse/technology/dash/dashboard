#!/usr/bin/php
<?php 	
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

require_once (dirname(__FILE__) . '/debug.php');
trace_file_info(__FILE__);

function bootstrap() {
	global $_pathsToProjects;
	global $_namespaceToProjects;
	
	if ($_pathsToProjects) return;
	
	parseProjects("gatherProjectDownload", "gatherProjectNamespace");
}

function gatherProjectDownload($project, $path) {
	global $_pathsToProjects;
	
	$_pathsToProjects[$path] = $project;
}

function gatherProjectNamespace($project, $namespace) {
	global $_namespaceToProjects;
	
	trace("Project $project namespace includes '$namespace'");
	$_namespaceToProjects[$namespace] = $project;
}


function parseProjects($downloadFunction, $namespaceFunction) {
	$xml = simplexml_load_file("https://www.eclipse.org/projects/xml/projects.php");
	
	// Hack special cases...
	$downloadFunction("modeling.emf.mwe", "/home/data/httpd/download.eclipse.org/modeling/emft/mwe/");
//	$downloadFunction("modeling.emf.mwe", "/home/data/httpd/archive.eclipse.org/modeling/emft/mwe/");
	$namespaceFunction("modeling.emf.mwe", "org.eclipse.emf.mwe.");
	
	$downloadFunction("rt.gemini.web", "/home/data/httpd/download.eclipse.org/gemini.web");
	
	$namespaceFunction("rt.gemini.web", "org.eclipse.gemini.web");
	
	$downloadFunction("technology.packaging", "/home/data/httpd/download.eclipse.org/technology/epp");
	
	$downloadFunction("modeling.emft.emf-facet", '/home/data/httpd/download.eclipse.org/facet');
	$namespaceFunction('modeling.emft.emf-facet', 'org.eclipse.emf.facet');
	
	$downloadFunction("modeling.emfcompare", '/home/data/httpd/download.eclipse.org/modeling/emf/compare');
	$namespaceFunction('modeling.emfcompare', 'org.eclipse.emf.compare');

	$downloadFunction("modeling.gmf-notation", '/home/data/httpd/download.eclipse.org/modeling/gmp/gmf-notation');
	$downloadFunction("modeling.gmf-runtime", '/home/data/httpd/download.eclipse.org/modeling/gmp/gmf-runtime');
	$downloadFunction("modeling.gmf-tooling", '/home/data/httpd/download.eclipse.org/modeling/gmp/gmf-tooling');
	$namespaceFunction('modeling.gmf-notation', 'org.eclipse.gmf.notation');
	$namespaceFunction('modeling.gmf-runtime', 'org.eclipse.gmf.runtime');	
	$namespaceFunction('modeling.gmf-tooling', 'org.eclipse.gmf.tooling');
	$namespaceFunction('modeling.mmt.qvtd', 'org.eclipse.qvt');
	
	$downloadFunction("modeling.mmt.qvt-oml", '/home/data/httpd/download.eclipse.org/mmt/qvto');
	$namespaceFunction('modeling.mmt.qvt-oml', 'org.eclipse.m2m.qvt.oml');
	
	$downloadFunction("modeling.viatra", '/home/data/httpd/download.eclipse.org/viatra2');
	$namespaceFunction('modeling.viatra', 'org.eclipse.viatra2');
	
	$downloadFunction("tools.cdt.tcf", '/home/data/httpd/download.eclipse.org/tools/tcf');	
	$downloadFunction("technology.recommenders.incubator", "/home/data/httpd/download.eclipse.org/recommenders.incubator");
	
	parseProjectHierarchy($xml, $downloadFunction, $namespaceFunction);
}

function parseProjectHierarchy(&$parent, $downloadFunction, $namespaceFunction) {
	foreach ($parent->project as $project) {
		$id = (string)$project['id'];
		parseProjectHierarchy($project, $downloadFunction, $namespaceFunction);
		addProjectPaths($id, $downloadFunction, $namespaceFunction);
		/*
		 * Consider paths that are similar to the project name,
		 * e.g. 'modeling.emft.emf-client' is in downloads/emfclient
		 * To avoid duplication, only consider the simplified path
		 * if it is actually different from the original.
		 */
		$simple = preg_replace('/[_\-]/','',$id);
		if (strcmp($simple, $id) != 0)
		  addProjectPaths($simple, $downloadFunction, $namespaceFunction);
	}
}

function addProjectPaths(&$id, $downloadFunction, $namespaceFunction) {
	if ($id == 'modeling.emf') return; // skip container project
	if ($id == 'tools.mylyn') return;
	$segments = preg_split('/\./', $id);
	$paths = array();
	$count = 0;
	while ($count < count($segments)) {
		$path = '/home/data/httpd/download.eclipse.org/';
		$archive = '/home/data/httpd/archive.eclipse.org/';
		$namespace = 'org.eclipse.';
		for($index=$count;$index<count($segments);$index++) {
			$path .= $segments[$index] . '/';
			$archive .= $segments[$index] . '/';
			$namespace .= $segments[$index] . '.';
		}
		$count++;
		
		// Hack out the ones that can't possibly be right.
		if ($path == 'incubator') continue;
		if ($path == 'builds') continue;
		
		$downloadFunction($id, $path);
//		$downloadFunction($id, $archive);
		$namespaceFunction($id, $namespace);
	}
}

?>