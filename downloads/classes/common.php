<?php
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *******************************************************************************/

// TODO Merge with web-api/common.inc

require_once(dirname(__FILE__) . "/debug.php");
trace_file_info(__FILE__);

/*
 * The regular expression pattern that is used to determine whether
 * or not a project id is valid.
 * 
 * PRIVATE: THIS FIELD IS NOT API.
 */
$projectNameSegmentPattern = "[a-zA-Z0-9\\-]+";
$projectNamePattern = "$projectNameSegmentPattern(\\.$projectNameSegmentPattern){0,2}";
define('ProjectNamePattern', $projectNamePattern);

/**
 * Answers <code>true</code> if $name represents a valid project name; 
 * <code>false</code> otherwise.
 * 
 * @param string $name Must not be <code>null</code>
 * @return bool
 */
function is_valid_project_name($name) {
	global $projectNamePattern;
	
	return preg_match("/^$projectNamePattern$/", $name);
}

/**
 * Returns the name of the parent project, or <code>null</code> is there
 * is no parent project. Current implementation infers the name of the parent
 * from the given name by lopping off the last segment. e.g.
 * 
 * get_project_parent_id('eclipse.jdt.debugging') return 'eclipse.jdt'
 * .
 * @param string $name Must not be <code>null</code>
 * @return string
 */
function get_project_parent_id($name) {
	global $projectNamePattern, $projectNameSegmentPattern;
	preg_match("/^($projectNamePattern)\\.$projectNameSegmentPattern$/", $name, $matches);
	if (!$matches) return null;
	return $matches[1];
}

/**
 * Returns <code>true</code> if the second parameter represents a
 * valid subproject of the first parameter. Note that this only checks
 * to see if the names are compatible; no check is done to confirm that
 * either parameter represents the name of an actual existing project.

 * @param string $parent Must not be <code>null</code>
 * @param string $name Must not be <code>null</code>
 * @return bool
 */
function is_valid_subproject_id($parent, $name) {
	return preg_match("/^$parent\\./", $name);
}

/**
 * This function normalizes the provided URL to an HTTP form. Input
 * should be a valid eclipse.org URL or a relative URL (with or without
 * a leading slash).
 * 
 * e.g. The following URLs will all normalize to 
 * http://eclipse.org/woolsey/para.html
 * 
 * - http://www.eclipse.org/woolsey/para.html
 * - http://eclipse.org/woolsey/para.html
 * - http://localhost/woolsey/para.html
 * - woolsey/para.html 
 * - /woolsey/para.html)
 * 
 * Note that URLs that do not correspond to eclipse.org addresses, will
 * result in a <code>null</code> result.
 * 
 * Usage: 
 * 
 * normalizeHttpUrl('http://www.eclipse.org/woolsey/para.html');
 * 
 * @param string $url Must not be <code>null</code>
 * @return string
 */
function normalizeHttpUrl($url) {
	$pattern_wiki = "/^http:\\/\\/wiki\\.eclipse\\.org\\/.*$/";
	if (preg_match($pattern_wiki, $url)) return $url;
	
	$relative = normalizeRelativeUrl($url, 'www');
	if ($relative) return 'http://eclipse.org' . $relative;
	
	return null;
}

/**
 * This function normalizes the provided URL to valid file path on the
 * eclipse.org web directory. Input should be a valid eclipse.org URL 
 * or a relative URL (with or without a leading slash).
 * 
 * e.g. The following URLs will all normalize to 
 * /home/local/data/httpd/www.eclipse.org/html/woolsey/para.html
 * 
 * - http://www.eclipse.org/woolsey/para.html
 * - http://eclipse.org/woolsey/para.html
 * - http://localhost/woolsey/para.html
 * - woolsey/para.html 
 * - /woolsey/para.html)
 * 
 * Note that URLs that do not correspond to eclipse.org addresses, will
 * result in a <code>null</code> result.
 * 
 * Usage: 
 * 
 * normalizeFilePathUrl('http://www.eclipse.org/woolsey/para.html');
 * 
 * @param string $url Must not be <code>null</code>
 * @return string
 */
function normalizeFilePathUrl($url) {
	global $_SERVER;
	
	$relative = normalizeRelativeUrl($url);
	if (!$relative) return null;
	
	return $_SERVER['DOCUMENT_ROOT'] . $relative;
}
	
/**
 * This function normalizes the provided URL to a relative path. 
 * Input should be a valid eclipse.org URL or a relative URL 
 * (with or without a leading slash).
 * 
 * e.g. The following URLs will all normalize to 
 * /woolsey/para.html
 * 
 * - http://www.eclipse.org/woolsey/para.html
 * - http://eclipse.org/woolsey/para.html
 * - http://localhost/woolsey/para.html
 * - woolsey/para.html 
 * - /woolsey/para.html)
 * 
 * Note that URLs that do not correspond to eclipse.org addresses, will
 * result in a <code>null</code> result.
 * 
 * Usage: 
 * 
 * extractcRelativeUrl('http://www.eclipse.org/woolsey/para.html');
 * 
 * @param string $url Must not be <code>null</code>
 * @return string
 */
function normalizeRelativeUrl($url) {		
	if (!$url) return null;
	
	$url = trim($url);
	
	$pattern_word = '\w[\w-]*';
	$pattern_segment = "$pattern_word(\\.$pattern_word)*";		
	$pattern_relative_part = "$pattern_segment(\\/$pattern_segment)*\\/?"; 
	$pattern_relative_url = "/^\\/?($pattern_relative_part)$/";
	$pattern_http_url = "/^http:\\/\\/(www\\.)?eclipse\\.org\\/($pattern_relative_part)$/";
	$pattern_http_local_url = "/^http:\\/\\/localhost\\/($pattern_relative_part)$/";
	
	if (preg_match($pattern_relative_url, $url, $matches1)) {
		return '/' . $matches1[1];
	} else if (preg_match($pattern_http_url, $url, $matches2)) {
		return '/' . $matches2[2]; 
	} else if (preg_match($pattern_http_local_url, $url, $matches3)) {
		return '/' . $matches3[1];
	} else {
		trace("The url ($url) cannot be normalized.");
		return null;
	}
}
	
/**
 * This function forces a login event if the current user is not
 * already logged in or is logged in as a non-committer. This function
 * must be called before any HTML is written to the output stream.
 * 
 * This function assumes that the $App variable exists and has been
 * assigned an instance of the App class (from app.class.php).
 * 
 * @return void
 */
function login_committer() {
	global $App;
		
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/friends/friend.class.php");
		
	if ($App->devmode) return;
	
	$Session = $App->useSession("optional"); 
    $friend	 = $Session->getFriend();
    
    if (!$friend->getBugzillaID()) {
    	header("Location: " . LOGINPAGE . "?takemeback=" . $_SERVER['SCRIPT_URI']);
    	exit;
    }
	
    $trace = trace("User information");
    $trace->trace("Bugzilla ID: " . $Session->getBugzillaID());
		
    if (!$friend->getIsCommitter()) {	    	
    	header("Location: /projects/tools");
    	exit;
    }
    
    $trace->trace("User is a committer.");
}

/**
 * This function returns an instance of Friend corresponding to the
 * currently logged in committer, or null if nobody is logged in or
 * the currently logged in user is not a committer. Note that this
 * function may return a different instance of Friend on subsequent
 * calls (i.e. do not assume that the instance is cached; don't assume
 * that it isn't cached either, these things change).
 *  
 * This function assumes that the $App variable exists and has been
 * assigned an instance of the App class (from app.class.php).
 * 
 * @return Friend
 */
function get_committer() {
	global $App;
	
	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/classes/friends/friend.class.php");
	
	if ($App->devmode) {		
		$friend = new Friend();
		//$friend->setBugzillaID(12345);
		$friend->setFirstName("John");
		$friend->setLastName("Smith");
		
		return $friend;
	}
	
	$Session = $App->useSession("optional"); 
    $friend	 = $Session->getFriend();
    
    if (!$friend->getBugzillaID()) return null;
    if (!$friend->getIsCommitter()) return null;
    
    return $friend;
}

/**
 * This function returns a string that describes the current
 * "login" state, e.g. "You are logged in as Wayne Beaton."
 *
 * @return string
 */
function user_info_html() {
	
	$friend = get_committer();
	
	// TODO Need to sort out how to avoid hard-coding the URL.
	// There's a bit of a chicken-and-egg issue here. We don't get the
	// LOGINPAGE constant until the session.class.php file is loaded.
	// We don't load that file until we log in. That file can't load
	// in development mode. Yikes.
	$login = "https://dev.eclipse.org/site_login?takemeback=" . $_SERVER['SCRIPT_URI'];
	$logout = "https://dev.eclipse.org/site_login?submit=Logout&takemeback=" . $_SERVER['SCRIPT_URI'];
	
	if (!$friend) return "You must <a href=\"$login\">log in</a> as a committer to use these tools.";
	
	$first = $friend->getFirstName();
	$last = $friend->getLastName();
	$message = $friend->getIsCommitter() 
		? "<a href=\"$logout\">Log out</a>" 
		: " You are not a committer; you must <a href=\"$login\">log in</a> as a committer to use these tools";
	
	return "You are logged in as $first $last. $message."; 
}

/**
 * This function extracts the bundle name from the provided file name.
 * 
 * Example:
 * 
 * extract_bundle_name_and_version('javax.persistence.source_2.0.1.v201006031150.jar')
 *  returns 'javax.persistence.source_2.0.1'
 *  
 * Returns <code>null</code> if the bundle name cannot be determined.
 * 
 * @param string $name name of the file to extract from
 * @return string
 */
function extract_bundle_name_and_version($name) {
	if (preg_match('/^([a-zA-Z0-9\.-_]+[_-](\d+\.){3}).*\\.jar/', $name, $matches)) {
		return $matches[1];
	}
	if (preg_match('/^([a-zA-Z0-9\.-_]+[_-]([\d\.]*))[-_].*\\.jar/', $name, $matches)) {
		return $matches[1];
	}
	return null;
}

/**
 * This function tests the provided value to determine whether or
 * not is represents a well-formed project id. If the value fails the
 * test, a ValidationException is thrown. Callers can use this method
 * to make sure that their parameter values are reasonable.
 * 
 * @param string $id
 * @throws ValidationException
 * @return void
 */
function mustBeValidProjectId($id) {
	if (!is_valid_project_name($id)) throw new ValidationException("Valid project id expected.");
}

class ValidationException extends Exception {}

?>