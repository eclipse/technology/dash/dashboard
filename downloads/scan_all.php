#!/usr/bin/php
<?php 	
/*******************************************************************************
 * Copyright (c) 2011 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Wayne Beaton - initial API and implementation
 *******************************************************************************/

/**
 * This script generates the bash code needed to run an analysis of the
 * downloads server. It generates one file for each project that can be identified
 * on the download server.
 * 
 * Example usage:
 * 
 *  php /home/data/users/wbeaton/git/org.eclipse.dash.dashboard/downloads/scan_all.php | nohup nice bash &
 *  
 * This script is currently run every Friday afternoon on the build server
 * in a crontab job created and managed by wbeaton. 
 */
require_once (dirname(__FILE__) . '/classes/functions.php');
require_once (dirname(__FILE__) . '/classes/debug.php');
trace_file_info(__FILE__);

$tmp = "/tmp";
$home = dirname(__FILE__);
$findjars = "$home/findjars.sh";
/*
 * Add a grep pipe to the scan command to strip comments
 * For reasons I don't understand, the output files are being 
 * generated with '#!/usr/bin/php" on the first line.
 */
$scan = "php $home/scan.php | grep -v ^#";

$output = "/home/data/httpd/writable/projects/ip-scans";

echo "echo \"# Project/Directory mapping\" > $output/mapping.txt\n";
$map = array();
parseProjects("gatherDirectories", "doNothing");
foreach($map as $project => $directories) {
	$file = "$tmp/$project-list.txt";
	echo "echo -e '# File listing for $project.\\n' > $file\n";
	foreach ($directories as $directory) {
		echo "$findjars $directory >> $file \n";
		echo "echo \"$project, $directory\" >> $output/mapping.txt\n";
	}
	echo "cat $file | $scan > $output/$project.xml\n";
	echo "rm $file\n";
	
}

function gatherDirectories($project, $path) {
	if (!file_exists($path)) return;
	global $map;
	
	$map[$project][] = $path;
}

function doNothing($project, $namespace) {
}

?>